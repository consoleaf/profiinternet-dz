DROP PROCEDURE IF EXISTS submit_task;
CREATE PROCEDURE submit_task(_vk_uid integer, _task_id integer, filename TEXT, submitted_text LONGTEXT)
BEGIN
  IF filename = 'null' THEN
    SET filename = NULL;
  END IF;
  IF (SELECT task_id FROM users_tasks WHERE task_id = _task_id AND vk_uid = _vk_uid) IS NULL THEN
    INSERT INTO users_tasks(vk_uid, task_id, state, filename, admin_comment, submitted_text)
    VALUES (_vk_uid,_task_id,1,filename,'',submitted_text);
  ELSE
    UPDATE users_tasks
      SET
        `state` = 1,
        `filename` = filename,
        `submitted_text` = submitted_text
    WHERE `vk_uid` = _vk_uid AND task_id = _task_id;
  END IF;
END

