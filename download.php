<?php
$filename = $_GET['filename'];

session_start();

if (strpos($filename, "..") !== false || strpos($filename, "\0") !== false) {
    die("<h1>Forbidden.</h1> Гадкий хацкер -_-");
}

if (strpos($filename, "work") !== false && !$_SESSION["admin"]) {
    $uid = Array();
    preg_match("/files\/work\/[0-9]*\/([0-9]*)/", $filename, $uid);
    $uid = $uid[1];
    if ($uid != $_SESSION["uid"])
        die("<h1>Forbidden.</h1> Скачивать файлы могут только их владельцы.");
}

// нужен для Internet Explorer, иначе Content-Disposition игнорируется
if (ini_get('zlib.output_compression'))
    ini_set('zlib.output_compression', 'Off');

$file_extension = strtolower(substr(strrchr($filename, "."), 1));

if ($filename == "") {
    echo "ОШИБКА: не указано имя файла.";
    exit;
} elseif (!file_exists($filename)) // проверяем существует ли указанный файл
{
    echo "ОШИБКА: данного файла не существует.";
    exit;
};
switch ($file_extension) {
    case "pdf":
        $ctype = "application/pdf";
        break;
    case "exe":
        $ctype = "application/octet-stream";
        break;
    case "zip":
        $ctype = "application/zip";
        break;
    case "doc":
        $ctype = "application/msword";
        break;
    case "xls":
        $ctype = "application/vnd.ms-excel";
        break;
    case "ppt":
        $ctype = "application/vnd.ms-powerpoint";
        break;
    case "docx":
        $ctype = "application/msword";
        break;
    case "xlsx":
        $ctype = "application/vnd.ms-excel";
        break;
    case "pptx":
        $ctype = "application/vnd.ms-powerpoint";
        break;
    case "mp3":
        $ctype = "audio/mp3";
        break;
    case "gif":
        $ctype = "image/gif";
        break;
    case "png":
        $ctype = "image/png";
        break;
    case "jpeg":
    case "jpg":
        $ctype = "image/jpg";
        break;
    default:
        $ctype = "application/force-download";
}
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private", false); // нужен для некоторых браузеров
header("Content-Type: $ctype");
header("Content-Disposition: attachment; filename=\"" . basename($filename) . "\";");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . filesize($filename)); // необходимо доделать подсчет размера файла по абсолютному пути
readfile("$filename");
exit();
