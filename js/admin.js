function makeLinks(text) {
    return text.replace(
        /((http|https|ftp|ftps):\/\/[a-zA-Z0-9\-.]+\.[a-zA-Z]+(\/\S*)?)/g,
        '<a href="$1">$1</a>'
    );
}

function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

Vue.component("menu_templ", {
    template: "#menu_templ",
    props: ["buttons", "mode"],
    methods: {
        setMode: function (event) {
            var mode = event.target.hash.substr(1, event.target.hash.length - 1);
            Vue.set(app, "mode", mode);
        }
    }
});

Vue.component('task', {
    props: ["task"],
    template: "#task_template",
    methods: {
        show_admin_textarea: function (type) {
            if (this.committing === false || this.committing_type !== type) {
                this.committing = true;
                this.committing_type = type;
                return true;
            }
            return false;
        },
        commit: function (state) {
            var comment = this.admin_comment;
            $.ajax({
                url: "/admin/api/commit_task.php",
                method: "post",
                data: {
                    state: state,
                    comment: comment,
                    vk_uid: this.task.vk_uid,
                    task_id: this.task.task_id
                }
            })
        },
        commit_ok: function () {
            if (this.show_admin_textarea(0))
                return this.admin_comment = this.task.default_ok_text;
            this.commit(2);
            this.task.state = 2;
        },
        commit_not_ok: function () {
            if (this.show_admin_textarea(1))
                return this.admin_comment = this.task.default_not_ok_text;
            this.commit(3);
            this.task.state = 3;
        },
        makeLinks: makeLinks,
        urldecode: urldecode
    },
    computed: {
        shortfilename: function () {
            return this.task.filename.replace(/^.*[\\\/]/, '')
        }
    },
    data: function () {
        return {
            committing: false,
            admin_comment: "",
            committing_type: null
        }
    }
});

Vue.component('checkapp', {
    template: "#check_app",
    data: function () {
        return {
            tasks: [],
            state: "loading",
            cnt: 0
        }
    },
    computed: {
        isTasksEmpty: function () {
            return this.tasks.length === 0;
        },
        loaded_cnt: function () {
            return Math.max.apply(Math, Object.keys(this.tasks));
        }
    },
    created() {
        $.ajax({
            url: "/admin/api/to_check.php",
            data: {
                offset: 0
            },
            method: "post",
        }).done(function (data) {
            var json = JSON.parse(data);
            this.cnt = json.cnt;
            delete json.cnt;
            this.tasks = json;
            this.state = "loaded";
            if (this.tasks.length === 0)
                this.state = "empty";
        }.bind(this));
    },
    methods: {
        load: function () {
            $.ajax({
                url: "/admin/api/to_check.php",
                data: {
                    offset: this.loaded_cnt
                },
                method: "post",
            }).done(function (data) {
                var json = JSON.parse(data);
                this.cnt = json.cnt;
                delete json.cnt;
                this.tasks = json;
                this.state = "loaded";
                if (this.tasks.length === 0)
                    this.state = "empty";
            }.bind(this));
            this.$el.scrollTop = 0;
        },
        reload: function () {
            var offset = Math.min.apply(Math, Object.keys(this.tasks)) - 1;
            if (offset <= 0)
                offset = 0;
            $.ajax({
                url: "/admin/api/to_check.php",
                data: {
                    offset: offset
                },
                method: "post"
            }).done(function (data) {
                var json = JSON.parse(data);
                this.cnt = json.cnt;
                delete json.cnt;
                this.tasks = json;
                this.state = "loaded";
                if (this.tasks.length === 0)
                    this.state = "empty";
            }.bind(this));
            this.$el.scrollTop = 0;
        }
    }
});

Vue.component('task_stats', {
    template: "#task_stats_template",
    props: ["task", "task_id"],
    methods: {
        getLineStyle: function (state) {
            var state_colors = {
                1: "#FF7800",
                2: "#44A41F",
                3: "#D30000"
            };
            return {
                width: state.cnt / this.sum * 100 + "%",
                background: state_colors[state.state]
            }
        }
    },
    computed: {
        sum: function () {
            var res = 0;
            for (var i = -1; i < 5; i++)
                if (this.task[i])
                    res += parseInt(this.task[i].cnt);
            return res;
        }
    }
});

Vue.component('quiz_stats', {
    template: "#quiz_stats_template",
    props: ["quiz", "quiz_id"],
    methods: {
        getLineStyle: function (state) {
            var state_colors = {
                0: "#D30000",
                1: "#44A41F"
            };
            return {
                width: state.cnt / this.sum * 100 + "%",
                background: state_colors[state.passed]
            }
        }
    },
    computed: {
        sum: function () {
            var res = 0;
            for (var i = 0; i < 2; i++)
                if (this.quiz[i])
                    res += parseInt(this.quiz[i].cnt);
            return res;
        }
    }
});

Vue.component('statsapp', {
    template: "#stats_app",
    data: function () {
        return {
            tasks: [],
            quizes: [],
            state: "loading"
        }
    },
    methods: {
        reload: function () {
            $.ajax({
                url: "/admin/api/stats.php",
                method: "post"
            }).done(function (data) {
                var json = JSON.parse(data);
                this.tasks = json.tasks;
                this.quizes = json.quizes;
                this.state = "loaded";
                if (this.tasks.length === 0 && this.quizes.length === 0)
                    this.state = "empty";
            }.bind(this));
        }
    },
    created() {
        setInterval(this.reload, 60 * 1000);
        this.reload();
    }
});

Vue.component('user', {
    template: "#user_template",
    props: ["user"]
});

Vue.component("usersapp", {
    template: "#users_app",
    data: function () {
        return {
            users: [],
            query: ""
        }
    },
    created() {
        $.ajax({
            url: "/admin/api/filterUsers.php",
            method: "post",
            data: {
                pattern: this.query
            }
        }).done(function (data) {
            this.users = JSON.parse(data);
        }.bind(this));
    },
    watch: {
        query: function () {
            $.ajax({
                url: "/admin/api/filterUsers.php",
                method: "post",
                data: {
                    pattern: this.query
                }
            }).done(function (data) {
                this.users = JSON.parse(data);
            }.bind(this));
        }
    }
});

Vue.component("callsapp", {
    template: "#calls_app",
    data: function () {
        return {
            calls: [],
            states: {
                0: "Позвонить",
                1: "Не взял(а)",
                2: "Всё решили",
                3: "Не решили"
            }
        }
    },
    methods: {
        reload: function () {
            $.ajax({
                url: "/admin/api/get_calls_info.php"
            }).done(function (data) {
                var calls = JSON.parse(data);
                this.calls = {};
                calls.forEach(function (call) {
                    var ts = call.ts.split(" ")[0];
                    if (!this.calls[ts])
                        this.calls[ts] = [];
                    call.ts = call.ts.split(" ")[1].split(":");
                    call.ts = call.ts[0] + ":" + call.ts[1];
                    call.href = "//vk.com/id" + call.uid;
                    call.phonehref = "tel:" + call.phone;
                    call.cnt = parseInt(call.cnt);
                    this.calls[ts].push(call);
                }.bind(this))
            }.bind(this));
        },
        inc_cnt: function (call) {
            $.ajax({
                url: "/admin/api/modify_calls_info.php",
                data: {
                    id: call.id,
                    cnt: call.cnt + 1
                },
                method: "post"
            }).done(function (data) {
                this.reload();
            }.bind(this));
        },
        set_state: function (call, state) {
            $.ajax({
                url: "/admin/api/modify_calls_info.php",
                data: {
                    id: call.id,
                    state: state
                },
                method: "post"
            }).done(function (data) {
                this.reload();
            }.bind(this));
        }
    },
    created() {
        this.reload();
        setInterval(this.reload, 2 * 60 * 1000);
    }
});

Vue.component("settingsapp", {
    template: "#settings_app",
    data: function () {
        return {
            config: {}
        }
    },
    created() {
        this.reload();
        setInterval(this.reload, 2 * 60 * 1000);
    },
    methods: {
        confirmClearUsers: function () {
            $.confirm({
                title: "Подтвердите действие",
                content: "Действительно ли вы собираетесь удалить все данные о пользователях?",
                buttons: {
                    OK: {
                        text: "Да",
                        action: function () {
                            $.ajax({
                                url: "/admin/api/clearUsers.php",
                                method: "POST"
                            });
                        }.bind(this)
                    },
                    cancel: {
                        text: "Отмена"
                    }
                }
            })
        },
        reload: function () {
            $.ajax({
                url: "/admin/api/get_config.php"
            }).done(function (data) {
                var config = JSON.parse(data);
                config.hello_text = urldecode(config.hello_text);
                this.config = config;
            }.bind(this));
        },
        submit: function () {
            var config = JSON.parse(JSON.stringify(this.config));
            config.hello_text = encodeURIComponent(config.hello_text);
            $.ajax({
                url: "/admin/api/save_config.php",
                method: "post",
                data: config
            }).done(function (t) {
                this.reload();
            }.bind(this));
        }
    }
});

Vue.component("selectapp", {
    template: "#select_app",
    data: function () {
        return {
            filters: {},
            tasks: [],
            task: {},
            users: [],
            users_cnt: 0
        }
    },
    created() {
        $.ajax({
            url: "/admin/api/get_tasks.php"
        }).done(function (t) {
            this.tasks = JSON.parse(t);
            this.task = this.tasks[0];
        }.bind(this));
    },
    methods: {
        get_data: function () {
            $.ajax({
                url: "/admin/api/filter_utasks.php",
                data: {
                    filters: this.filters
                },
                method: "post"
            }).done(function (t) {
                var json = JSON.parse(t);
                this.users_cnt = json.cnt;
                this.users = json.users;
            }.bind(this));
        },
        change: function (id, value) {
            this.filters[id] = value;
        },
        submit_task: function () {
            var users = "";
            this.users.forEach(function (value) {
                users += value.vk_uid + " ";
            });
            $.ajax({
                url: "/admin/api/mass_submit.php",
                method: "post",
                data: {
                    uids: users,
                    task_id: this.task.task_id,
                    message: this.task.default_ok_text
                }
            }).done(function (t) {
                new Noty({
                    layout: 'bottomRight',
                    text: 'Запрос успешно выполнен.'
                }).show();
            }.bind(this));
        }
    }
});

Vue.component("editapp", {
    template: "#edit_app",
    data: function () {
        return {
            tasks: {}
        }
    },
    methods: {
        reload: function () {
            $.ajax({
                url: "/admin/api/get_tasks.php"
            }).done(function (t) {
                this.tasks = JSON.parse(t);
            }.bind(this));
        }
    },
    created() {
        this.reload();
    }
});

Vue.component("editor", {
    template: "#editor_template",
    props: ["task"],
    data: function () {
        return {
            newfile: "Загрузить файл...",
            noFileWarning: false
        }
    },
    computed: {
        files: function () {
            var files = [];
            var _files = this.task.files.split(" ");
            _files.forEach(function (value, index) {
                if (urldecode(value))
                    files.push(urldecode(value));
            });
            return files;
        }
    },
    methods: {
        submit: function () {
            var task = JSON.parse(JSON.stringify(this.task));
            task.blocked = task.blocked * 1;
            var formdata = new FormData();
            for (var key in task) {
                formdata.append(key, task[key]);
            }
            if (this.newfile !== "Загрузить файл...") {
                formdata.append("upload_file", $("#newfile" + this.task.task_id).prop("files")[0]);
            }
            formdata.append("mode", "edit_task");
            $.ajax({
                url: "/admin/api/edit_task.php",
                method: "post",
                data: formdata,
                processData: false,
                contentType: false
            }).done(function (t) {
                this.$parent.reload();
                $("#newfile" + this.task.task_id)[0].value = "";
                this.newfile = "Загрузить файл..."
            }.bind(this));
        },
        watchNewFile: function (event) {
            this.newfile = event.target.files[0].name;
        },
        // upload_file: function () {
        //     if (this.newfile === "Загрузить файл...") {
        //         this.noFileWarning = true;
        //         setTimeout(function () {
        //             this.noFileWarning = false;
        //         }.bind(this), 2000);
        //         return
        //     }
        //     var file_data = $("#newfile" + this.task.task_id).prop("files")[0];
        //     var form_data = new FormData();
        //     form_data.append()
        // }
    },
    created() {
        if ((typeof this.task.blocked) === "string")
            this.task.blocked = parseInt(this.task.blocked);
    },
    updated() {
        if ((typeof this.task.blocked) === "string")
            this.task.blocked = parseInt(this.task.blocked);
    }
});

Vue.component("file", {
    template: "#file_template",
    props: ["file", "task"],
    methods: {
        deleteFile: function () {
            var url = '/admin/api/edit_task.php';
            $.ajax({
                url: url,
                data: {
                    mode: "delete_file",
                    task_id: this.task.task_id,
                    filename: this.file
                }
            }).done(function () {
                this.$parent.$parent.reload();
            }.bind(this));
        }
    }
});

Vue.component("quizapp", {
    template: "#quiz_app",
    data: function () {
        return {
            quiz: {}
        }
    },
    created() {
        this.reload();
        setInterval(this.reload, 2 * 60 * 1000);
    },
    methods: {
        reload: function () {
            $.ajax({
                url: "/admin/api/get_quiz.php"
            }).done(function (data) {
                this.quiz = JSON.parse(data);
            }.bind(this));
        },
        submit: function () {
            var quiz = JSON.parse(JSON.stringify(this.quiz));
            $.ajax({
                url: "/admin/api/edit_quiz.php",
                method: "post",
                data: quiz
            }).done(function (t) {
                this.reload();
            }.bind(this));
        }
    }
});

var app = new Vue({
    el: "#main_container",
    data: {
        mode: window.location.hash.substr(1, window.location.hash.length - 1),
        modes: {
            check: "Проверка заданий",
            stats: "Просмотр статистики",
            select: "Profi Internet",
            edit: "Редактирование заданий",
            calls: "Запрошенные звонки",
            settings: "Параметры",
            users: "Пользователи",
            quiz: "Тест"
        }
    },
    created() {
        if (!this.mode)
            this.mode = "check";
    },
    methods: {}
});

new ClipboardJS('.clipboard');
