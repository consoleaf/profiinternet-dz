<?php

session_start();

$login = "vetal-lt";
$password = "qwertydsa";

if (isset($_REQUEST["login"]) && $_REQUEST["login"] == $login and $_REQUEST["password"] == $password) {
    $_SESSION["admin"] = true;
} else if (isset($_REQUEST['login']))
    echo "Неверный логин и/или пароль!<br>";

if (!isset($_SESSION["admin"]) || !$_SESSION['admin']) {
    ?>

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Авторизуйтесь</title>
        <link rel="stylesheet" href="/css/auth.css">
    </head>
    <body>
    <div>
        <form action="#" method="post">
            <div class="group">
                <input type="text" title="Введите ваш пароль." id="login" name="login" required>
                <span class="bar"></span>
                <label>Логин</label>
            </div>

            <div class="group">
                <input type="password" title="Введите ваш пароль." id="password" name="password" required>
                <span class="bar"></span>
                <label>Пароль</label>
            </div>

            <input type="submit" class="button">
        </form>
    </div>

    </body>
    </html>

    <?php

    exit;
}
