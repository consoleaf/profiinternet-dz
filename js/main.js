var task_icons = {
    "-1": "замок_з",
    "0": "замок_о",
    "1": "часы",
    "2": "галка",
    "3": "крестик"
};

var task_states = {
    "-1": "lock",
    "0": "available",
    "1": "checking",
    "2": "done",
    "3": "rejected"
};

var current_task_id = "1";
var current_quiz_id = "1";

for (var key in tasks) {
    // skip loop if the property is from prototype
    if (!tasks.hasOwnProperty(key)) continue;

    var obj = tasks[key];
    for (var prop in obj) {
        // skip loop if the property is from prototype
        if (!obj.hasOwnProperty(prop)) continue;

        // your code
        if (obj.state >= 0)
            current_task_id = obj.task_id;
    }
}

function urldecode(url) {
    return decodeURIComponent(url.replace(/\+/g, ' '));
}


var app = new Vue({
    el: "#main-container",
    data: {
        user: user,
        tasks: tasks,
        quizes: quizes,
        task_states: task_states,
        task_icons: task_icons,
        current_task_id: current_task_id,
        current_quiz_id: current_quiz_id,
        mode: 'task',
        filename: "Загрузить файл..."
    },
    computed: {
        current_task: function () {
            var task = this.tasks[this.current_task_id];
            if (task.state === "-1" && task.task_id == 1)
                task.state = "0";
            if (task.blocked === "1") {
                task.descr = task.lock_descr;
                if (task.state == 0)
                    task.state = "-1";
            }
            return task;
        },
        current_quiz: function () {
            return quizes ? this.quizes[this.current_quiz_id] : {score: 0, ms: 100};
        },
        btn1_width: function () {
            return document.getElementById("button1").clientWidth + "px"
        },
        showCrown: function () {
            if (this.current_quiz)
                return parseInt(this.current_quiz.score) >= parseInt(this.current_quiz.ms);
            else
                return false;
        }
    },
    methods: {
        refreshFileSpan: function(e) {
            var filename = e.currentTarget.value;
            filename = filename.split("\\");
            filename = filename[filename.length - 1];
            this.filename = filename;

        },
        taskClass: function (task) {
            var res = ["task", this.task_states[task.state]];
            if (task.task_id === this.current_task_id && this.mode === 'task')
                res.push("active");
            return res;
        },
        makeTaskActive: function (task) {
            if (task.state === "-1")
                return;
            this.current_task_id = task.task_id;
            this.mode = 'task'
        },
        urldecode: urldecode,
        quiz_available: function (quiz) {
            var max_done_task_id = 0;
            $.each(this.tasks, function (key, value) {
                    // console.log(value);
                    if (value.state === "2")
                        max_done_task_id = parseInt(value.task_id);
                }
            );
            return max_done_task_id >= quiz.mt;
        },
        file_icon: function (filename) {
            var type = filename.split(".");
            type = type[type.length - 1];

            var types = {
                "xls": "xlsx",
                "xlsx": 'xlsx',
                'doc': 'docx',
                'docx': "docx",
                'rar': 'zip',
                'zip': 'zip',
                'ppt': 'pptx',
                'pptx': 'pptx',
                'pdf': 'pdf'
            };

            if (!types[type])
                types[type] = "url";

            return '<img src="/res/file_icons/' + types[type] + '.png">';
        },
        quiz_icon: function (score, ms) {
            score = parseInt(score);
            ms = parseInt(ms);
            var res = 0;
            if (score >= ms)
                res = 2;
            if (score < ms && score !== "-1")
                res = 3;
            if (score < 0)
                res = 0;
            return "/res/иконки/" + task_icons[res] + ".png";
        }
    },
    created() {
        $("#main-container")[0].classList.remove("hidden");
    }
});

$("#button3").click(function (e) {
    e.preventDefault();
    $.confirm({
        backgroundDismiss: true,
        useBootstrap: false,
        boxWidth: "23em",
        title: "Введите номер:",
        content: "<i>Ожидайте звонок в промежуток с 14:00 по 17:00 МСК.</i>" +
        "<br><select name='length' id='length'>" +
        "<option value='+9 (999) 999-99-99'>+0 (000) 000-00-00</option>" +
        "<option value='+9 (999) 999-99-99-9'>+0 (000) 000-00-00-0</option>" +
        "<option value='+9 (999) 999-99-99-99'>+0 (000) 000-00-00-00</option>" +
        "<option value='+9 (999) 999-99-99-999'>+0 (000) 000-00-00-000</option>" +
        "<option value='+9 (999) 999-99-99-9999'>+0 (000) 000-00-00-0000</option>" +
        "</select><br><input style='width: calc(100% - 45px); font-size: 25px; margin: 20px' id='phone' placeholder='+0 (000) 000-00-00'>",
        buttons: {
            OK: {
                text: "Заказать звонок от наставника",
                btnClass: "btn-green phone-cnfrm-btn",
                action: function () {
                    if (document.getElementById("phone").value.length < 18)
                        return false;
                    $.ajax({
                        url: "/request_call.php",
                        method: "post",
                        data: {"phone": document.getElementById("phone").value},
                        success: function (data) {
                            if (data.length === 0)
                                $.confirm({
                                    title: "Готово!",
                                    content: "Заказ звонка выполнен успешно.",
                                    useBootstrap: false,
                                    boxWidth: "25rem",
                                    backgroundDismiss: true,
                                    escapeKey: true,
                                    buttons: {
                                        OK: {}
                                    }
                                })
                        }
                    });
                }
            },
            Cancel: {
                text: "Отмена",
                btnClass: "btn-red"
            }
        },
        onOpen: function () {
            var phone = $("#phone");
            $('select').on('change', function (e) {
                var optionSelected = $("option:selected", this);
                var valueSelected = this.value;

                // console.log(e);

                phone.mask(valueSelected);
                phone[0].placeholder = optionSelected[0].innerHTML;

                phone[0].focus()
            })
        },
        onOpenBefore: function () {
            var phone = $("#phone");
            phone.mask("+9 (999) 999-99-99");
            phone.keyup(function (e) {

                if (e.keyCode === 13)
                    document.querySelector(".phone-cnfrm-btn").click();

                /*var value = document.getElementById("phone").value;

                if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) {

                    // 0-9 only

                    //check length of number
                    var cnt = 0;

                    for (var i = 0; i < document.getElementById("phone").value.length; i++) {
                        if ("0123456789".indexOf(document.getElementById("phone").value[i]) !== -1) {
                            cnt++;
                        }
                    }

                    if (cnt >= 11 && cnt < 15) {
                        switch (cnt) {
                            case 11:
                                phone.mask("+9 (999) 999-99-99-9");
                                break;
                            case 12:
                                phone.mask("+9 (999) 999-99-99-99");
                                break;
                            case 13:
                                phone.mask("+9 (999) 999-99-99-99-9");
                                break;
                            case 14:
                                phone.mask("+9 (999) 999-99-99-99-99");
                                break;
                            default:
                                break;
                        }

                        for (var i = 0; i < value.length; i++) {
                            simulateKeyPress(phone, value[i]);
                        }

                        simulated = true;
                        simulateKeyPress(String.fromCharCode(e.keyCode));
                        simulated = false;
                    }
                }*/
            })
        }
    });
});
