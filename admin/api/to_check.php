<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 14.07.18
 * Time: 17:12
 */

require_once "../mysql_login.php";

$offset = 0;
if (isset($_POST["offset"]))
    $offset = $_POST["offset"];

$result = mysqli_query($link, "
                SELECT users.`vk_uid`,tasks.`task_id`,`name`,`title`,`submitted_text`,`filename`,`admin_comment`, `ts`,
                       `avatar_uri`, `default_ok_text`, `default_not_ok_text`, `state`
                FROM users_tasks
                  JOIN tasks
                    ON tasks.task_id = users_tasks.task_id
                  JOIN users
                    ON users_tasks.vk_uid = users.vk_uid 
                WHERE `state` = 1
                ORDER BY tasks.task_id ASC, ts DESC
                LIMIT 10
                OFFSET $offset;");
$cnt = mysqli_fetch_assoc(mysqli_query($link, "SELECT COUNT(ts) cnt FROM users_tasks WHERE state = 1"))["cnt"];
$array = Array();

$i = $offset + 1;
while ($row = mysqli_fetch_assoc($result))
    $array[$i++] = $row;

$array["cnt"] = $cnt;

echo json_encode($array);