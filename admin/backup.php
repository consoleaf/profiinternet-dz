<?php

require_once "auth.php";

function rcopy($src, $dst)
{
    $dir = opendir($src);
    @mkdir($dst);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            if (is_dir($src . '/' . $file)) {
                rcopy($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . "/" . $object))
                    rrmdir($dir . "/" . $object);
                else
                    unlink($dir . "/" . $object);
            }
        }
        rmdir($dir);
    }
}

function Zip($source, $destination)
{
    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true) {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file) {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
                continue;

            $file = realpath($file);

            if (is_dir($file) === true) {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            } else if (is_file($file) === true) {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    } else if (is_file($source) === true) {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

    return $zip->close();
}

//rename("files", "oldfiles");

rcopy("files", "oldfiles");

include "mysql_login.php";

$tasks = mysqli_query($link, "SELECT * FROM tasks");

while ($task = mysqli_fetch_assoc($tasks)) {
    $path = "oldfiles/task/" . $task["task_id"] . "/";
    $text = $task["title"] . "\n\n--------------------------------------\n\n" . $task["descr"];
    if (!file_exists($path))
        mkdir($path);
    file_put_contents($path . "задание.txt", $text);
}

$users_tasks = mysqli_query($link, "SELECT task_id, users.vk_uid uid, state, name, submitted_text, admin_comment 
                                          FROM users_tasks JOIN users ON users.vk_uid = users_tasks.vk_uid");

$results = Array(-1 => "недоступно", 0 => "доступно", 1 => "на проверке", 2 => "принято", 3 => "отказано");

while ($utask = mysqli_fetch_assoc($users_tasks)) {
    $path = "oldfiles/work/" . $utask["task_id"] . "/" . $utask["uid"] . "/";
//    echo $path . "<br>";
    $result = $results[$utask["state"]];
    $text = $utask["name"] . "\n\n" . urldecode($utask["submitted_text"]) . "\n\n--------------------------------------\n\n" .
        urldecode($utask["admin_comment"]) . "\n\nРезультат: $result\n";

    if (!file_exists($path))
        mkdir($path);

    file_put_contents($path . "ответ_и_комментарий.txt", $text);
}

Zip("oldfiles", "backup.zip");

rrmdir("oldfiles");

?> Готово! <a href="backup.zip">backup.zip</a>
