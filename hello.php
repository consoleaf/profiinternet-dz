<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 15.01.18
 * Time: 20:29
 */


include "admin/mysql_login.php";

$result = mysqli_query($link, "SELECT hello_text,iframe_link FROM config");
$row = mysqli_fetch_assoc($result);
$iframe = urldecode($row["iframe_link"]);
$text = urldecode($row["hello_text"]);


?>
<!doctype html>
<html lang="ru">
<head>
    <?php
    header("X-Frame-Options: ALLOWALL");
    ?>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/hello.css">
    <title>Домашние задания</title>
</head>
<body>
<div id="main_cont">
    <img src="res/logo_logo.png" alt="" id="logo">
    <div id="text"><?php echo $text; ?></div>
    <div id="iframe">
        <iframe width="560" height="315" src="<?php echo $iframe; ?>" frameborder="0" allowfullscreen></iframe>
    </div>
    <a href="/main.php" style="text-decoration: none">
        <div id="to_main" class="button">Приступить к выполнению ДЗ</div>
    </a>
</div>
</body>
</html>
