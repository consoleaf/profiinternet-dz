<?php

require_once "auth.php";

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . "/" . $object))
                    rrmdir($dir . "/" . $object);
                else
                    unlink($dir . "/" . $object);
            }
        }
        rmdir($dir);
    }
}


include "mysql_login.php";

if ($_REQUEST["mode"] == '0') {
    rrmdir("files/task");
    mysqli_query($link, "DELETE FROM `tasks`");
    mysqli_query($link, "DELETE FROM `config`");
    mysqli_query($link, "INSERT INTO `config`() VALUES()");
}
rrmdir("files/work");
mysqli_query($link, "DELETE FROM `users`");

?>
<script>document.location = "/admin.php";</script>
