<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 14.05.18
 * Time: 0:27
 */


set_error_handler('err_handler');
function err_handler(/** @noinspection PhpUnusedParameterInspection */
    $errno, $errmsg, $filename, $linenum)
{
    /** @noinspection PhpUnusedLocalVariableInspection */
    $date = date('Y-m-d H:i:s (T)');
    $f = fopen('errors.txt', 'a');
    if (!empty($f)) {
        $filename = str_replace($_SERVER['DOCUMENT_ROOT'], '', $filename);
        $err = "$errmsg = $filename = $linenum\r\n";
        fwrite($f, $err);
        fclose($f);
    }
}

session_start();
require_once "admin/mysql_login.php";

$quiz_id = $_GET["id"];
$uid = $_SESSION["uid"];
if (!isset($_SESSION["uid"]))
    $uid = $_GET["uid"];
$done = mysqli_query($link, "SELECT quiz_id FROM users_quizes WHERE vk_uid = $uid AND quiz_id = $quiz_id");
$done = mysqli_num_rows($done) ? true : false;
$score = NULL;
$min_score = NULL;

// Check if user is permitted to do the test

$result = mysqli_query($link, "SELECT vk_uid FROM users_tasks JOIN quizes ON min_task_done = task_id AND vk_uid = $uid AND state = 2");
if (!mysqli_num_rows($result))
    die("На данный момент Вам недоступен тест.");

if ($done) {
    $result = mysqli_query($link, "SELECT score, min_score FROM users_quizes JOIN quizes ON users_quizes.quiz_id = quizes.quiz_id WHERE users_quizes.quiz_id = $quiz_id AND vk_uid = $uid");
    $result = mysqli_fetch_assoc($result);
    $score = $result["score"];
    $min_score = $result["min_score"];
}

$my_answers = Array();
$correct_answers = Array();
if ($done) {
    $answers_result = mysqli_query($link, "SELECT answer_id FROM users_answers WHERE vk_uid = $uid AND quiz_id = $quiz_id");
    while ($answer = mysqli_fetch_assoc($answers_result)) {
        array_push($my_answers, $answer["answer_id"]);
    }
    $correct_result = mysqli_query($link, "SELECT correct_answer_id FROM quizes_questions");
    while ($answer = mysqli_fetch_assoc($correct_result)) {
        array_push($correct_answers, $answer["correct_answer_id"]);
    }
}

//print_r($correct_answers);

$result = mysqli_query($link, "SELECT question_id, question_text" .
    " FROM quizes_questions WHERE quiz_id = $quiz_id");

$question = null;
$cnt = 1;
$max_score = mysqli_num_rows($result);

if ($done)
    echo "<h2>Ваш результат: " . $score . "/" . $max_score . ". Вы " . ($score >= $min_score ? "успешно сдали" : "не прошли") . " тест.</h2>";

?>
<form id="quiz" action="/submit_quiz.php" method="post">
    <input type="hidden" name="quiz_id" value="<?php echo $quiz_id ?>">
    <input type="hidden" name="uid" value="<?php echo $uid; ?>">
    <?php
    while ($question = mysqli_fetch_assoc($result)) {
        ?>
        <div class="question notchosen">
            <h2><?php echo $cnt++, ". ", $question["question_text"]; ?> </h2>
            <?php
            $question_id = $question["question_id"];
            $answers = mysqli_query($link, "SELECT answer_id, answer_text FROM questions_answers WHERE question_id = $question_id");
            $answer = null;
            for ($i = 0; $i < mysqli_num_rows($answers); $i++) {
                $answer = mysqli_fetch_assoc($answers)
                ?>
                <label class="answer <?php if (in_array($answer["answer_id"], $my_answers)) echo "selected" ?> <?php if ($done && in_array($answer["answer_id"], $correct_answers)) echo "correct" ?>"
                       for="answer<?php echo $answer["answer_id"]; ?>">
                    <input type="radio" required <?php if ($done) {
                        echo "disabled";
                    } ?> name="question[<?php echo $question["question_id"]; ?>]"
                           id="answer<?php echo $answer["answer_id"]; ?>"
                           value="<?php echo $answer["answer_id"]; ?>" <?php if (in_array($answer["answer_id"], $my_answers)) {
                        echo "checked";
                    } ?>>
                    <span><?php echo $answer["answer_text"]; ?></span>
                </label>
                <?php
            }
            ?>
        </div>
        <?php
    }
    ?>
    <input type="submit" value="Отправить" id="submitter">
</form>

<style>
    @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,600|Open+Sans:300,350,400');

    * {
        font-family: "Open Sans", sans-serif;
        transition: all 0.2s ease-in;
    }

    input[type=radio] {
        display: none;
    }

    .answer {
        display: block;
        margin: 10px;
    }

    #submitter {
        margin-top: 1em;
    }

    .answer {
        display: block;
        padding: 0.7em;
        cursor: pointer;
    }

    .answer:hover {
        /*font-weight: bold;*/
        background-color: #d4d4d4;
    }

    .answer.selected {
        background-color: #bbb;
        width: calc(100% - 72px);
        transform: translateX(30px);
    }

    .notchosen .selected {
        background-color: #fa706b;
    }

    .correct {
        background-color: #9fff8b !important;
    }

    .selected.correct {
        background-color: #74e160 !important;
    }

    .require, .require * {
        color: darkred !important;
    }
</style>

<script src="/js/jquery-3.2.1.min.js"></script>
<script>
    $(function () {
        if (<?php echo($done ? 1 : 0) ?>) {
            $("input").toArray().forEach(function (value) {
                value.disabled = true;
            })
        }
    });

    $("input[type='radio']").change(function (e) {
        var name = e.currentTarget.name;

        $('input[name="' + name + '"]').toArray().forEach(function (value, index, array) {
            value.parentNode.classList.remove("selected");
        });

        // console.log(e.currentTarget.parentNode)
        e.currentTarget.parentNode.classList.add("selected");
        e.currentTarget.parentNode.parentNode.classList.remove("notchosen");
        e.currentTarget.parentNode.parentNode.classList.remove("require");
    });

    $("#submitter").click(function (event) {
        event.preventDefault();
        var checked = $(".selected").toArray().length;
        var cnt = <?php echo $cnt ? $cnt : 0; ?> -1;
        if (checked !== cnt) {
            $(".notchosen").toArray().forEach(function (value, index, array) {
                value.classList.add("require");
            });
            var offset = $(document.querySelector(".notchosen")).offset();
            offset.left -= 20;
            offset.top -= 20;
            $('html, body').animate({
                scrollTop: offset.top,
                scrollLeft: offset.left
            });
        } else {
            $("form").submit();
        }
    })
</script>
