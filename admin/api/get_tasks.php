<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 22.07.18
 * Time: 14:00
 */

require_once "../auth.php";
require_once "../mysql_login.php";

$result = mysqli_query($link, "SELECT * FROM tasks");

$json = Array();

while ($row = mysqli_fetch_assoc($result))
    array_push($json, $row);

echo json_encode($json);