<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 02.04.18
 * Time: 13:07
 */

require_once "../auth.php";
require_once "../mysql_login.php";

$keys = Array(
    "0" => "SELECT vk_uid FROM users",
    "1" => "SELECT vk_uid FROM users_tasks WHERE state = 1 AND task_id = {}",
    "2" => "SELECT vk_uid FROM users_tasks WHERE state = 2 AND task_id = {}",
    "3" => "SELECT vk_uid FROM users_tasks WHERE state = 3 AND task_id = {} UNION SELECT vk_uid FROM users_tasks WHERE vk_uid NOT IN (SELECT vk_uid FROM users_tasks WHERE state != 3 AND task_id = {})",
    "12" => "SELECT vk_uid FROM users_tasks WHERE (state = 1 OR state = 2) AND task_id = {}"
);

$request = "SELECT DISTINCT vk_uid, name FROM users WHERE 1";

$filters = $_POST["filters"];
foreach ($filters as $task_id => $filter) {
    $state = $filter;
    if ($state != "-")
        $request .= " AND vk_uid IN " . str_replace("{}", $task_id, "(" . $keys[$state] . ")");
}

//print_r($request);

$result = mysqli_query($link, $request);

$users = Array();

while ($row = mysqli_fetch_assoc($result))
    array_push($users, $row);

$res = Array(
    "cnt" => count($users),
    "users" => $users
);

echo json_encode($res);

?>
