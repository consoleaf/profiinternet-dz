<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 17.07.18
 * Time: 20:50
 */

require_once "../mysql_login.php";
require_once "../auth.php";

$call_rq_states = Array(
    0 => "Позвонить",
    1 => "Не взял(а)",
    2 => "Всё решили",
    3 => "Не решили"
);

$result = mysqli_query($link, "SELECT * FROM call_requests ORDER BY ts DESC");

$json = Array();

while ($row = mysqli_fetch_assoc($result))
    array_push($json, $row);

echo json_encode($json);

