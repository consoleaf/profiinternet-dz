<?php

set_error_handler('err_handler');
function err_handler(/** @noinspection PhpUnusedParameterInspection */
    $errno, $errmsg, $filename, $linenum)
{
//    $date = date('Y-m-d H:i:s (T)');
    $f = fopen('errors.log', 'a');
    if (!empty($f)) {
        $filename = str_replace($_SERVER['DOCUMENT_ROOT'], '', $filename);
        $time = date("Y-m-d H-M-s");
        $err = "[$time]  $errmsg = $filename = $linenum\r\n";
        fwrite($f, $err);
        fclose($f);
    }
}

function make_links_clickable($content)
{

    // The link list
    $links = array();

    // Links out of text links
    preg_match_all('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', $content, $matches);
    foreach ($matches[0] as $key => $link) {
        $links[$link] = $link;
    }

    // Get existing
    preg_match_all('/<a\s[^>]*href=([\"\']??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU', $content, $matches);
    foreach ($matches[2] as $key => $value) {
        if (isset($links[$value])) {
            unset($links[$value]);
        }
    }

    // Replace in content
    foreach ($links as $key => $link) {
        $content = str_replace($link, '<a href="' . $link . '" target="_blank">' . $link . '</a>', $content);
    }

//    $content = iconv("UTF-8","UTF-8", $content);
    return $content;
}

function make_YT_iframe($content)
{

    // The link list
    $links = array();

    // Links out of text links
    preg_match_all('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', $content, $matches);
    foreach ($matches[0] as $key => $link) {
        $links[$link] = $link;
    }

    // Get existing
    preg_match_all('/<*\s[^>]*=([\"\']??)([^\" >]*?)\\1[^>]*>(.*)<\/*>/siU', $content, $matches);
    foreach ($matches[2] as $key => $value) {
        if (isset($links[$value])) {
            unset($links[$value]);
        }
    }

    // Replace in content
    foreach ($links as $key => $link) {
        if (strpos($link, 'youtube.com/watch?') === false)
            continue;

        $oldlink = $link;
        $link = explode('=', $link);
        $link = $link[1];
        $content = str_replace($oldlink, '<iframe class="YT" src="https:\/\/youtube.com\/embed\/' . $link . '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>', $content);
    }

//    $content = iconv("UTF-8","UTF-8", $content);
    return $content;
}

session_start();
include "admin/mysql_login.php";

$vk_uid = $_SESSION["uid"];
setcookie('uid', $vk_uid, 0, "/");

$result = mysqli_query($link, "SELECT name,avatar_uri FROM users WHERE vk_uid = {$vk_uid}");

if ($user = mysqli_fetch_assoc($result)) {
    ?>
    <!doctype html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Домашние задания</title>
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/jquery-confirm.css">
    </head>
    <body>

    <div id="main-container" class="container hidden">
        <div id="left_half">
            <div id="user">
                <div id="avatardiv"><a :href="'https://vk.com/id' + user.uid"><img id="avatar" :src="user.avatar_uri"
                                                                                   alt=""></a>
                    <img v-if="showCrown" src="res/logo.png"
                         id="bant" title="Вы УСПЕШНО сдали тест, с чем мы и хотим поздравить 🎉
Так как Вы успешно сдали тест, у Вас будет возможность участвовать в нашей 3-х месячный онлайн-программе «СПЕЦИАЛИСТ ПО ИНТЕРНЕТ-РЕКЛАМЕ 3.0» Подробнее на 5 (финальном) занятии">
                </div>
                <div id="username">{{ user.name.split(" ")[1] + " " + user.name.split(" ")[0] }}</div>
                <a href="/logout.php"><img src="/res/иконки/дверь.png" alt="" class="clickable"></a>
            </div>
            <div id="tasks">
                <a v-for="task in tasks" :href="task.state >= 0 ? '#right_half' : '#'"
                   :class="taskClass(task)"
                   @click="makeTaskActive(task)"
                >
                    <div>{{ task.title }}</div>
                    <div class="icon"><img :src="'res/иконки/' + task_icons[task.state] + '.png'" alt=""></div>
                </a>
            </div>
            <div id="quizes" v-if="quizes">
                <a
                        v-for="quiz in quizes"
                        :class="'task ' + (quiz.score > -1 ? 'done' : 'available') + (quiz.id === current_quiz_id && mode == 'quiz' ? ' active' : '')"
                        @click="current_quiz_id = quiz.id; mode='quiz'"
                        v-if="quiz_available(quiz)"
                >
                    <div>{{ quiz.title }}</div>
                    <div class="icon"><img :src="quiz_icon(quiz.score, quiz.ms)"
                                           alt=""></div>
                </a>
            </div>
        </div>
        <div id="right_half">
            <div id="header">
                <div id="title"><img src="/res/logo_logo.png" alt=""></div>
                <div id="buttondiv">
                    <a :href="current_task.button1_url" class="button" target="_blank" id="button1">
                        {{ current_task.button1_text }}</a>
                    <div id="button1_descr" :style="'width: ' + btn1_width">{{ current_task.button1_descr }}</div>
                </div>
            </div>
            <div id="task" :class="(mode == 'task' ? '' : 'hidden')">
                <form action="submit.php" method="POST" enctype="multipart/form-data">
                    <h2 id="task_title">{{ current_task.title }}</h2>
                    <div id="task_description" v-html="current_task.descr"></div>
                    <div id="task_files" class="block_div" v-if="current_task.blocked === '0' && current_task.files[0] !== ''">
                        <name>Дополнительный материал:</name>
                        <div>
                            <div class='filelink' v-for="file in current_task.files" v-if="file">
                                <a :href='"/download.php?filename=files/task/" + current_task.task_id + "/" + urldecode(file)'
                                   v-html="file_icon(urldecode(file))">
                                </a>
                                <div>{{ urldecode(file) }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="block_div"
                         v-if="current_task.state > 1 && current_task.admin_comment"
                         id="admin_comment"
                         :style="'color: ' + (current_task.state == 2 ? 'darkgreen' : 'darkred')">
                        <name>Комментарий преподавателя</name>
                        <div>{{ current_task.admin_comment }}</div>
                    </div>
                    <input type="hidden" name="task_id" id="task_id" :value="current_task.task_id">
                    <div id="textarea_div" class="block_div">
                        <name><label for="submit_text">Введите текст</label></name>
                        <div><textarea name="text" id="submit_text" cols="100"
                                       rows="20" :disabled="current_task.blocked == true || current_task.state == 2">{{ urldecode(current_task.submitted_text) }}</textarea><br>
                        </div>
                    </div>

                    <div class="block_div" id="file_div">
                        <name>Загрузка файла</name>
                        <div>
                            <label id="filelabel" for="newfile" class="upload clickable"><img
                                        src="res/иконки/скрепка.png"
                                        alt="">
                                <span v-html="filename"></span>
                            </label>
                            <input type="file" title="Загрузить файл..." name="file" id="newfile" class="hidden" @change="refreshFileSpan">
                            <a :href="'/download.php?filename=' + current_task.filename" style="margin-top: 10px;"
                               id="submitted_file" v-if="current_task.filename">Загруженный
                                ранее файл</a>
                        </div>
                    </div>
                    <label for="submit" class="button clickable">Отправить</label>
                    <input type="hidden" name="vk_uid" :value="user.uid">
                    <input class="hidden" type="submit" id="submit">
                </form>
            </div>
            <div id="quiz" :class="(mode == 'quiz' ? '' : 'hidden')" v-if="current_quiz_id !== -1">
                <h2 id="quiz_title">{{ current_quiz.title }}</h2>
                <span class="warning">(за распространение ответов мы исключаем из курса)</span>
                <iframe :src="'/quiz.php?id=' + current_quiz.id" frameborder="0" id="quiz_frame"></iframe>
            </div>
        </div>
    </div>


    <?php
    $result = mysqli_query($link, "SELECT button2_url,button2_text FROM config");
    $button = mysqli_fetch_assoc($result);
    ?>
    <a href="<?php echo $button["button2_url"]; ?>" id="button2" class="clickable hidden"
       target="_blank" title="Задать вопрос"><img src="res/иконки/помощь.png" alt=""></a>

    <a href="<?php echo $button["button2_url"]; ?>" id="button3" @click="requestCall" class="clickable"
       target="_blank" title="Заказать звонок"><img src="res/иконки/звонок.png" alt=""></a>

    <script>
        var user = {
            name: "<?php echo $user["name"]; ?>",
            avatar_uri: "<?php echo $user["avatar_uri"]; ?>",
            uid: "<?php echo $vk_uid; ?>"
        };

        var tasks = <?php

            $result = mysqli_query($link, "SELECT
                                                  tasks.task_id,
                                                  title,
                                                  descr,
                                                  lock_descr,
                                                  state,
                                                  files,
                                                  admin_comment,
                                                  submitted_text,
                                                  filename,
                                                  button1_url,
                                                  button1_text,
                                                  button1_descr,
                                                  blocked
                                                FROM tasks
                                                  RIGHT JOIN users_tasks ON tasks.task_id = users_tasks.task_id
                                                WHERE users_tasks.vk_uid = {$vk_uid}
                                                UNION
                                                SELECT task_id,title,descr,lock_descr,-1,files,'','','',
                                                       button1_url,button1_text,button1_descr, blocked
                                                FROM tasks");

            $json = Array();

            $used = Array();

            $prev_done = false;

            $max_task = -1;

            while ($row = mysqli_fetch_assoc($result)) {
                if (!in_array($row["task_id"], $used)) {
                    array_push($used, $row["task_id"]);
                    if ($prev_done && $row['state'] == '-1')
                        $row['state'] = 0;
                    $row["descr"] = make_links_clickable(make_YT_iframe($row['descr']));
                    $row["files"] = explode(" ", $row["files"]);
                    $json[$row["task_id"]] = $row;
                    $json["admin_comment"] = urldecode($json["admin_comment"]);
                    $prev_done = ($row['state'] == 2);
                    if ($row['state'] == 2)
                        $max_task = max($max_task, $row["task_id"]);
                }
            }

            echo json_encode($json);
            ?>;

        <?php

        $result = mysqli_query($link, "SELECT

                                                quizes.quiz_id id, quiz_title title, u.score score, min_score ms, min_task_done mt
                                                  
                                                FROM quizes
                                                  RIGHT JOIN users_quizes u ON quizes.quiz_id = u.quiz_id
                                                WHERE u.vk_uid = $vk_uid AND min_task_done <= $max_task
                                                UNION
                                                SELECT
                                                
                                                quiz_id id, quiz_title title, -1 score, min_score ms, min_task_done mt
                                                
                                                FROM quizes
                                                ");

        $json = Array();
        $used = Array();

        while ($row = mysqli_fetch_assoc($result)) {
            if (in_array($row["id"], $used))
                continue;
            array_push($used, $row['id']);
            $json[$row['id']] = $row;
        }



        ?>

        var current_quiz_id = <?php echo empty($used) ? 0 : $used[0]; ?>;

        <?php if (!empty($used)) { ?>
        var quizes = <?php echo json_encode($json); ?>;
        <?php } else { ?>
        var quizes = {};
        <?php } ?>

    </script>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="js/fontawesome.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/jquery-confirm.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/main.js"></script>

    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?158"></script>
    <!-- VK Widget -->
    <div id="vk_community_messages"></div>
    <script type="text/javascript">
        VK.Widgets.CommunityMessages("vk_community_messages", 156417267, {tooltipButtonText: "Есть вопрос? Будем рады помочь!"});
    </script>
    </body>
    </html>
<?php } else {
    ?>
    <script>document.location = "/logout.php";</script><h1>Переадресация</h1>
    <?php
}

mysqli_close($link);