<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 23.07.18
 * Time: 15:30
 */

require_once "../auth.php";
require_once "../mysql_login.php";

$result = mysqli_query($link, "SELECT * FROM quizes");

echo json_encode(mysqli_fetch_assoc($result));