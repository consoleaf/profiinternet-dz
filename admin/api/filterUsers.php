<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 17.07.18
 * Time: 21:21
 */

require_once "../mysql_login.php";
require_once "../auth.php";

$pattern = $_POST["pattern"];

$usernames = mysqli_query($link, "SELECT
  vk_uid,
  name,
  avatar_uri,
  email
FROM users
  LEFT JOIN email_accounts ON users.vk_uid = email_accounts.uid
WHERE name LIKE '%$pattern%' OR email LIKE '%$pattern%' OR vk_uid LIKE '%$pattern%'
ORDER BY name
LIMIT 100");

$json = Array();

while ($row = mysqli_fetch_assoc($usernames))
    array_push($json, $row);

echo json_encode($json);