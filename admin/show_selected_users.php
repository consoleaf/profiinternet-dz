<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 02.04.18
 * Time: 13:07
 */

require_once "mysql_login.php";
require_once "auth.php";

$keys = Array(
    "2" => "SELECT vk_uid FROM users_tasks WHERE state = 2 AND task_id = {}",
    "1" => "SELECT vk_uid FROM users_tasks WHERE state = 1 AND task_id = {}",
    "1,2" => "SELECT vk_uid FROM users_tasks WHERE (state = 2 OR state = 1) AND task_id = {}",
    "0,3" => "SELECT vk_uid FROM users_tasks WHERE state = 3 AND task_id = {}
UNION
SELECT vk_uid FROM users_tasks WHERE vk_uid NOT IN (SELECT vk_uid FROM users_tasks WHERE state != 3 AND task_id = {})"
);

$request = "SELECT vk_uid FROM users WHERE 1";

$settings = $_REQUEST["data"];
foreach ($settings as $setting) {
    $setting = explode("|", $setting);
    $id = $setting[0];
    $state = $setting[1];
    if ($state != "-")
        $request .= " AND vk_uid IN " . str_replace("{}", $id, "(" . $keys[$state] . ")");
}

$result = mysqli_query($link, $request);
$uids = "";
while ($uid = mysqli_fetch_assoc($result)["vk_uid"])
    $uids .= $uid . "<br>";

$res = Array(
    "cnt" => substr_count($uids, "<br>"),
    "html" => $uids
);

echo json_encode($res);

?>
