<?php

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . "/" . $object))
                    rrmdir($dir . "/" . $object);
                else
                    unlink($dir . "/" . $object);
            }
        }
        rmdir($dir);
    }
}

session_start();



$vk_uid = $_SESSION["uid"];
if (!$vk_uid)
    $vk_uid = $_REQUEST["vk_uid"];
$task_id = $_REQUEST["task_id"];
$submitted_text = urlencode($_REQUEST["text"]);
$filename = null;


include "admin/mysql_login.php";

$res = mysqli_query($link, "SELECT blocked FROM tasks WHERE task_id = $task_id");

$blocked = mysqli_fetch_assoc($res);

$resume = true;
if ($blocked["blocked"] == 1)
    $resume = false;

if (!empty($_FILES)) {
    $name = $_FILES["file"]["name"];
    $tmp_name = $_FILES["file"]["tmp_name"];

//    if (file_exists("files/work/$task_id/$vk_uid"))
//        rrmdir("files/work/$task_id/$vk_uid");
    if (!file_exists("files/work/$task_id/$vk_uid"))
        mkdir("files/work/$task_id/$vk_uid", 0777, true);

    if (move_uploaded_file($tmp_name, "files/work/$task_id/$vk_uid/$name"))
        $filename = "files/work/$task_id/$vk_uid/$name";
}

$query = "SELECT task_id FROM users_tasks WHERE vk_uid = $vk_uid AND state != 3 ORDER BY task_id DESC LIMIT 1";
$result = mysqli_query($link, $query);
$last_task_id = mysqli_fetch_assoc($result)["task_id"];

if (!$last_task_id)
    $last_task_id = -1;


$query2 = "SELECT task_id FROM tasks WHERE task_id > $last_task_id ORDER BY task_id ASC LIMIT 1";
$result2 = mysqli_query($link, $query2);
$new_task_id = mysqli_fetch_assoc($result2)["task_id"];

if ($new_task_id != $task_id && $new_task_id != null)
    $resume = false;

$query3 = "CALL submit_task($vk_uid, $task_id, '$filename', '$submitted_text')";
if ($resume) {
    $result3 = mysqli_query($link, $query3);
} else {
    $result = false;
}

file_put_contents('sqllog.txt', mysqli_error($link) . PHP_EOL, FILE_APPEND | LOCK_EX);
if (mysqli_error($link)) {
    file_put_contents('sqllog.txt', date("d-m-y H:i:s") . PHP_EOL, FILE_APPEND | LOCK_EX);
    file_put_contents('sqllog.txt', $query . PHP_EOL, FILE_APPEND | LOCK_EX);
    file_put_contents('sqllog.txt', $query2 . PHP_EOL, FILE_APPEND | LOCK_EX);
    file_put_contents('sqllog.txt', $query3 . PHP_EOL, FILE_APPEND | LOCK_EX);
}


?>
<script>document.location = "main.php?<?php echo($result3 ? "success" : "success") ?>=1";</script>
