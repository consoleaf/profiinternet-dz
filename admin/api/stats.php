<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 17.07.18
 * Time: 16:38
 */

require_once "../mysql_login.php";
require_once "../auth.php";

$result = mysqli_query($link, "SELECT count(vk_uid) cnt, task_id, state, max(ts) last_ts 
FROM users_tasks GROUP BY task_id, state
");

$result1 = mysqli_query($link, "SELECT q.quiz_id quiz_id, count(vk_uid) cnt, (uq.score >= q.min_score) passed 
FROM users_quizes uq JOIN quizes q ON q.quiz_id = uq.quiz_id GROUP BY passed ORDER BY passed DESC;");


$json = Array();

$tasks_stats = Array();

while ($row = mysqli_fetch_assoc($result)) {
    if (!isset($tasks_stats[$row["task_id"]]))
        $tasks_stats[$row["task_id"]] = Array();
    $tasks_stats[$row["task_id"]][$row["state"]] = $row;
}

$quiz_stats = Array();

while ($row = mysqli_fetch_assoc($result1)) {
    if (!isset($quiz_stats[$row["quiz_id"]]))
        $quiz_stats[$row["quiz_id"]] = Array();
    array_push($quiz_stats[$row["quiz_id"]], $row);
}

$json["tasks"] = $tasks_stats;
$json["quizes"] = $quiz_stats;

echo json_encode($json);