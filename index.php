<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Войдите в систему</title>
    <link rel="stylesheet" href="css/index.css">
    <link rel="icon" type="image/ico" href="/favicon.ico">
    <link rel="stylesheet" href="css/jquery-confirm.css">
</head>
<body>
<div id="container">
    <img src="/res/logo_logo.png" alt="" id="logo">
    <div id="text">Бесплатный 2-недельный онлайн курс <br>&laquo;Специалист по Интернет-рекламе&raquo;</div>
    <div id="label">Чтобы приступить к курсу, нажмите кнопку ниже</div>


    <a id="button"
       href="https://oauth.vk.com/authorize?client_id=6350866&scope=1&redirect_uri=https://profiinternet-dz.ru/vklogin.php&response_type=code"
       style="text-decoration: none">
        <div class="button">ВОЙТИ ЧЕРЕЗ <img src="res/253788.png" alt=""></div>
    </a>
    <div id="cant_login">Не удаётся войти?</div>
    <a id="email_login">Вход по почте</a>
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/jquery-confirm.js"></script>
<script>
    $("#cant_login").click(function () {
        $.confirm({
            useBootstrap: false,
            boxWidth: "50%",
            escapeKey: true,
            backgroundDismiss: true,
            title: "Не удаётся войти?",
            content: "Если возникли проблемы с входом в <a href='https://profiinternet-dz.ru'>личный кабинет</a>:<br>" +
            "1) Выдаёт ошибку: {\"error\":\"invalid_request\",\"error_description\":\"Security Error\"}<br>" +
            "2) Пустая станица на странице с домашним заданием.<br>" +
            "<br>" +
            "<b>I)Инструкция у кого НЕТ браузера Google Chrome!</b><br>" +
            "Вам необходимо скачать браузер Google Chrome по ссылке: <a href='https://www.google.com/chrome/'>https://www.google.com/chrome/</a><br>" +
            "1)<a href='http://prntscr.com/i8m6uy'>http://prntscr.com/i8m6uy</a><br>" +
            "2)<a href='http://prntscr.com/i8m72x'>http://prntscr.com/i8m72x</a><br>" +
            "Устанавливаете его и запускаете.<br>" +
            "1) Переходите на наш сайт: <a href='https://profiinternet-dz.ru'>https://profiinternet-dz.ru</a><br>" +
            "2) Нажимаете сюда: <a href='http://prntscr.com/i8mad8'>http://prntscr.com/i8mad8</a><br>" +
            "3) Вводите логин и пароль от Вконтакте: <a href='http://prntscr.com/i8mc8'>http://prntscr.com/i8mc8</a><br>" +
            "Если у Вас не появилось окно ввода логина и пароля, Вам необходимо зайти на сайт <a href='https://vk.com'>vk.com</a> " +
            "и выйти с Вашего аккаунта: <a href='http://prntscr.com/i8mf4f'>http://prntscr.com/i8mf4f</a> И заново повторяете с шага № 1" +
            " (Переходите на наш сайт: <a href='https://profiinternet-dz.ru'>https://profiinternet-dz.ru</a>)<br>" +
            "4) Можете посмотреть ознакомительное видео по работе в личном кабинете и нажимаете " +
            "\"Приступить к выполнению дз\": <a href='http://prntscr.com/i8mdlw'>http://prntscr.com/i8mdlw</a><br>" +
            "<br>" +
            "II) <b>Инструкция у кого ЕСТЬ браузер Google Chrome!</b><br>" +
            "1) Переходите на наш сайт: <a href='https://profiinternet-dz.ru'>https://profiinternet-dz.ru</a><br>" +
            "2) Нажимаете сюда: <a href='http://prntscr.com/i8mad8'>http://prntscr.com/i8mad8</a><br>" +
            "3) Вводите логин и пароль от Вконтакте: <a href='http://prntscr.com/i8mc8'>http://prntscr.com/i8mc8</a><br>" +
            "Если у Вас не появилось окно ввода логина и пароля, Вам необходимо зайти на сайт <a href='https://vk.com'>vk.com</a> и выйти с Вашего аккаунта: <a href='http://prntscr.com/i8mf4f'>http://prntscr.com/i8mf4f</a> И заново повторяете с шага № 1 (Переходите на наш сайт: <a href='https://profiinternet-dz.ru'>https://profiinternet-dz.ru</a>)<br>" +
            "4) Можете посмотреть ознакомительное видео по работе в личном кабинете и нажимаете \"Приступить к выполнению дз\": <a href='http://prntscr.com/i8mdlw'>http://prntscr.com/i8mdlw</a><br>" +
            "Если ошибка всё равно остаётся, Вам необходимо зайти с другого браузера. Если проблема осталась, напишите Виталию в службу поддержки: <a href='https://vk.com/profiinternet_sp'>https://vk.com/profiinternet_sp</a>",
            buttons: {
                OK: {}
            }
        })
    });

    $("#email_login").click(function () {
        $.confirm({
            boxWidth: "310px",
            useBootstrap: false,
            backgroundDismiss: true,
            escapeKey: true,
            title: "Авторизация/регистрация",
            content: "url:/email_form.php",
            buttons: {
                login: {
                    text: "Вход",
                    btnClass: "btn-green",
                    action: login,
                },
                register: {
                    text: "Регистрация",
                    btnClass: "btn-blue",
                    action: register,
                },
                cancel: {
                    text: "Отмена",
                }
            },
            onContentReady: function () {
                $("#password").keydown(function (e) {
                    if (e.keyCode === 13) {
                        e.preventDefault();
                        login()
                    }
                })
            }
        });
    });

    function login() {
        var email = document.querySelector("#email");
        var password = document.querySelector("#password");
        if (!email.value || !password.value) {
            $("#required").show();
            setTimeout(function () {
                $("#required").hide();
            }, 5000);
            return false;
        }
        $.ajax({
            url: "/login.php",
            method: "post",
            data: {
                "email": email.value,
                "password": password.value
            },
            complete: function (data) {
                data = JSON.parse(data.responseText);
                if (data["success"] === 0) {
                    $("#wrong").show();
                    return;
                }
                window.location = "/hello.php"
            }
        });
        return false;
    }

    function register() {
        var email = document.querySelector("#email");
        var password = document.querySelector("#password");
        if (!email.value || !password.value) {
            $("#required").show();
            setTimeout(function () {
                $("#required").hide();
            }, 5000);
            return false;
        }
        $.confirm({
            boxWidth: "310px",
            useBootstrap: false,
            backgroundDismiss: true,
            escapeKey: true,
            title: "Регистрация",
            content: "url:/register_form.html",
            onContentReady: function () {
                $("#new_name").keydown(function (e) {
                    if (e.keyCode === 13) {
                        e.preventDefault();
                        if (!email.value || !password.value) {
                            $("#required").show();
                            setTimeout(function () {
                                $("#required").hide();
                            }, 5000);
                            return false;
                        }
                        complete_register(email, password);
                    }
                })
            },
            buttons: {
                OK: {
                    action: function () {
                        complete_register(email, password);
                        return false;
                    }
                }
            }
        });
        return false;
    }

    function complete_register(email, password) {
        var name = document.querySelector("#new_name");

        if (!name.value) {
            $("#required_name").show();
            setTimeout(function () {
                $("#required_name").hide();
            }, 5000);
            return false;
        }

        $.ajax({
            url: "/register.php",
            method: "post",
            data: {
                "email": email.value,
                "password": password.value,
                "name": name.value
            },
            complete: function (data) {
                data = JSON.parse(data.responseText);
                if (data["success"] === 1) {
                    window.location = "/hello.php";
                }
                if (data["success"] === 0) {
                    $("#exists").show();
                }
            }
        });

        return false;
    }
</script>
</body>
</html>
