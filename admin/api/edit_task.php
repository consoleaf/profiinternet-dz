<?php

include "../mysql_login.php";
require_once "../auth.php";

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . "/" . $object))
                    rrmdir($dir . "/" . $object);
                else
                    unlink($dir . "/" . $object);
            }
        }
        rmdir($dir);
    }
}

$result = null;
//echo $_REQUEST["mode"];
if ($_REQUEST["mode"] == "edit_task") {
    $task_id = $_REQUEST["task_id"];
    $title = str_replace("'", "\\'", $_REQUEST["title"]);
    $descr = str_replace("'", "\\'", $_REQUEST["descr"]);
    $default_ok_text = str_replace("'", "\\'", $_REQUEST["default_ok_text"]);
    $default_not_ok_text = str_replace("'", "\\'", $_REQUEST["default_not_ok_text"]);
    $lock_descr = str_replace("'", "\\'", $_REQUEST["lock_descr"]);
    $btn_text = str_replace("'", "\\'", $_REQUEST["button1_text"]);
    $btn_url = str_replace("'", "\\'", $_REQUEST["button1_url"]);
    $btn_descr = str_replace("'", "\\'", $_REQUEST["button1_descr"]);
    $blocked = $_REQUEST["blocked"];
    if ($blocked)
        $blocked = 1;
    else
        $blocked = 0;

    $filename = null;
    if (!empty($_FILES)) {
        $name = $_FILES["upload_file"]["name"];
        $tmp_name = $_FILES["upload_file"]["tmp_name"];
//        echo $name . "; " . $tmp_name;

        if (!file_exists("../../files/task/$task_id/"))
            mkdir("../../files/task/$task_id", 0777, true);

        if (move_uploaded_file($tmp_name, "../../files/task/$task_id/$name"))
            $filename = $name;
        else
            echo "failed";


    }

    print_r($_FILES);
    echo $filename;

    $result = mysqli_query($link, "SELECT `files` FROM tasks WHERE `task_id` = $task_id");

    $files = mysqli_fetch_assoc($result)["files"];

    if ($filename) {
        if (strlen($files) > 1)
            $files .= " ";
        $files = $files . urlencode($filename);
    }

    $result = mysqli_query($link, "UPDATE tasks 
SET `title` = '$title', `descr` = '$descr', `lock_descr` = '$lock_descr', `files` = '$files',
    `button1_text` = '$btn_text', `button1_url` = '$btn_url', `button1_descr` = '$btn_descr', 
    `blocked` = $blocked, `default_ok_text` = '$default_ok_text', `default_not_ok_text` = '$default_not_ok_text'
WHERE `task_id` = $task_id");

    if (!$result)
        echo "UPDATE tasks 
SET `title` = '$title', `descr` = '$descr', `lock_descr` = '$lock_descr', `files` = '$files',
    `button1_text` = '$btn_text', `button1_url` = '$btn_url', `button1_descr` = '$btn_descr', 
    `blocked` = $blocked, `default_ok_text` = '$default_ok_text', `default_not_ok_text` = '$default_not_ok_text'
WHERE `task_id` = $task_id";
}

if ($_REQUEST["mode"] == "new_task") {
    //TODO

//    $task_id = $_REQUEST["task_id"];
    $title = str_replace("'", "\\'", $_REQUEST["title"]);
    $descr = str_replace("'", "\\'", $_REQUEST["descr"]);
    $lock_descr = str_replace("'", "\\'", $_REQUEST["lock_descr"]);
    $btn_text = str_replace("'", "\\'", $_REQUEST["button1_text"]);
    $btn_url = str_replace("'", "\\'", $_REQUEST["button1_url"]);

    $result = mysqli_query($link, "SELECT max(task_id) as task_id FROM tasks");
    $task_id = mysqli_fetch_assoc($result)["task_id"] + 1;
    $blocked = $_REQUEST["blocked"];
    if ($blocked)
        $blocked = 1;
    else
        $blocked = 0;

    $filename = null;
    if (!empty($_FILES)) {
        $name = $_FILES["upload_file"]["name"];
        $tmp_name = $_FILES["upload_file"]["tmp_name"];

        if (!file_exists("../../files/task/$task_id/"))
            mkdir("../../files/task/$task_id", 0777, true);

        if (move_uploaded_file($tmp_name, "../../files/task/$task_id/$name"))
            $filename = $name;


    }

//    print_r($_FILES);
//    echo $filename;

    $files = urlencode($filename);

    $result = mysqli_query($link, "INSERT INTO tasks(task_id, title, descr, lock_descr, files, button1_url, button1_text, button1_descr, blocked) 
                                          VALUES ($task_id, '$title', '$descr', '$lock_descr', '$files', '$btn_url', '$btn_url', '$btn_descr', $blocked)");
}
if ($_REQUEST["mode"] == "delete_file") {

    $task_id = $_REQUEST["task_id"];
    $filename = $_REQUEST["filename"];


    $result = mysqli_query($link, "SELECT `files` FROM tasks WHERE `task_id` = $task_id");

    $files = explode(" ", mysqli_fetch_assoc($result)["files"]);

    $newfiles = "";
    foreach ($files as $file) {
        $file = urldecode($file);
        if ($file != $filename) {
            if (strlen($newfiles))
                $newfiles .= " ";
            $newfiles .= urlencode($file);
        }
    }

    $result = mysqli_query($link, "UPDATE tasks SET `files` = '$newfiles' WHERE `task_id` = $task_id");
//    echo mysqli_error($link);

    if ($result)
        unlink("../../files/task/$task_id/$filename");
}
if ($_REQUEST["mode"] == "delete_task") {
    $task_id = $_REQUEST["task_id"];

    $result = mysqli_query($link, "DELETE FROM tasks WHERE task_id = $task_id");
    $result = mysqli_query($link, "DELETE FROM users_tasks WHERE task_id = $task_id");

    rrmdir("../../files/task/$task_id");
}


