<?php


include "admin/auth.php";
include "admin/mysql_login.php";


function make_links_clickable($content)
{

    // The link list
    $links = array();

    // Links out of text links
    preg_match_all('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', $content, $matches);
    foreach ($matches[0] as $key => $link) {
        $links[$link] = $link;
    }

    // Get existing
    preg_match_all('/<a\s[^>]*href=([\"\']??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU', $content, $matches);
    foreach ($matches[2] as $key => $value) {
        if (isset($links[$value])) {
            unset($links[$value]);
        }
    }

    // Replace in content
    foreach ($links as $key => $link) {
        $content = str_replace($link, '<a href="' . $link . '" target="_blank">' . $link . '</a>', $content);
    }

//    $content = iconv("UTF-8","UTF-8", $content);
    return $content;
}


?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админка</title>
    <link rel="stylesheet" href="css/jquery-confirm.css">
    <link rel="stylesheet" href="css/oldadmin.css">
</head>
<body>
<div id="header">
    <a href="/main.php" target="_blank">
        <div class="link">Смотреть сайт</div>
    </a>
    <div class="link" id="checking_button">Сданные задания</div>
    <div class="link" id="stats">Статистика</div>
    <div class="link" id="select">Выборка</div>
    <div class="link" id="editing_button">Редактирование заданий</div>
    <div class="link" id="call_requests">Запрошенные звонки</div>
    <div class="link" id="settings_button">Настройки</div>
</div>
<div id="main_container">
    <div id="checking">
        <h2>Проверка сданных заданий</h2>

        <table>
            <?php

            if ($_GET["page"])
                $page = $_GET["page"];
            else
                $page = 1;

            $cnt_on_page = 10;

            $offset = ($page - 1) * $cnt_on_page;

            $result = mysqli_query($link, "
                SELECT users.`vk_uid`,tasks.`task_id`,`name`,`title`,`submitted_text`,`filename`,`admin_comment`, `ts`
                FROM users_tasks
                  JOIN tasks
                    ON tasks.task_id = users_tasks.task_id
                  JOIN users
                    ON users_tasks.vk_uid = users.vk_uid
                WHERE `state` = 1
                ORDER BY tasks.task_id ASC, ts DESC
                LIMIT " . $offset . ", $cnt_on_page");


            while ($unit = mysqli_fetch_assoc($result)) {

                $filename = explode("/", $unit["filename"]);
                $filename = $filename[count($filename) - 1];
                $task_id = $unit["task_id"];
                $vk_uid = $unit["vk_uid"];
                $ts = $unit["ts"];

                ?>
                <tr class="unit">
                    <td class="ts"><?php echo $ts; ?></td>
                    <td class="name"><a href="https://vk.com/id<?php echo $vk_uid; ?>"><?php echo $unit["name"]; ?></a>
                    </td>
                    <td class="title"><?php echo $unit["title"]; ?></td>
                    <td class="text">
                        <span><?php echo make_links_clickable(urldecode($unit["submitted_text"])); ?></span></td>
                    <td class="file"><a
                                href="/download.php?filename=<?php echo $unit["filename"]; ?>"><?php echo $filename; ?></a>
                    </td>
                    <td class="checks">
                        <a href="#" class="result" data-task_id="<?php echo $task_id; ?>"
                           data-vk_uid="<?php echo $vk_uid; ?>" data-result="1">Зачёт</a>
                        &nbsp;/&nbsp;
                        <a href="#" class="result" data-task_id="<?php echo $task_id; ?>"
                           data-vk_uid="<?php echo $vk_uid; ?>" data-result="0">Незачёт</a>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <div id="pages">
            <?php if ($page > 1) { ?>
                <a href="?page=<?php echo $page - 1; ?>">Назад</a>
            <?php } ?>
            <b>&nbsp;&nbsp;<?php echo $page; ?>&nbsp;&nbsp;</b>
            <a href="?page=<?php echo $page + 1; ?>">Вперёд</a>
        </div>
    </div>
    <div id="editing" style="display: none;">
        <h2>Редактирование заданий</h2>
        <?php
        $result = mysqli_query($link, "SELECT task_id,title,descr,lock_descr,files,button1_text,button1_url,button1_descr,blocked FROM tasks");

        while ($task = mysqli_fetch_assoc($result)) {

            $task_id = $task["task_id"];

            ?>

            <form action="admin/edit.php" method="POST" enctype="multipart/form-data">
                <div class="task">
                    <input type="hidden" name="mode" value="edit_task">
                    <input type="hidden" name="task_id" value="<?php echo $task["task_id"]; ?>">
                    <div class="title_div">
                        <span>Заголовок:</span>
                        <input type="text" name="title" value="<?php echo $task["title"]; ?>">
                    </div>
                    <div class="textarea_div">
                        <span>Описание: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button
                                    class="makeBold">Полужирный</button></span>
                        <textarea name="descr" cols="30" rows="10"><?php echo $task["descr"]; ?></textarea>
                    </div>
                    <div class="textarea_div">
                        <span>Описание при блокировке: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button
                                    class="makeBold">Полужирный</button></span>
                        <textarea name="lock_descr" cols="30" rows="10"><?php echo $task["lock_descr"]; ?></textarea>
                    </div>
                    <div class="upload_file">
                        <span>Загрузить ещё один файл:</span>
                        <input type="file" name="upload_file">
                    </div>

                    <div class="button_config">
                        <span>Надпись на верхней кнопке:</span>
                        <input type="text" name="btn1_text" id="btn1_text" value="<?php echo $task["button1_text"]; ?>">
                        <span>Ссылка на верхней кнопке:</span>
                        <input type="text" name="btn1_link" id="btn1_link" value="<?php echo $task["button1_url"]; ?>">
                        <span>Текст под кнопкой:</span>
                        <input type="text" name="btn1_descr" id="btn1_descr"
                               value="<?php echo $task["button1_descr"]; ?>">
                    </div>

                    <div style="margin: 1em 0;">
                        <input type="checkbox" <?php echo $task["blocked"] ? "checked" : ""; ?> name="blocked">
                        <label for="blocked">Заблокировать</label>
                    </div>

                    <div class="submit_div">
                        <input type="submit">
                    </div>

                </div>
            </form>
            <div class="files">
                <?php
                $files = explode(' ', $task["files"]);

                if (strlen($task["files"]) > 1)
                    foreach ($files as $file) {
                        $file = urldecode($file);
                        ?>
                        <a href="<?php echo "/files/task/$task_id/$file"; ?>"><?php echo $file; ?></a>&nbsp;&nbsp;
                        <a href="<?php echo "/admin/edit.php?mode=delete_file&task_id=$task_id&filename=$file"; ?>">(удалить)</a>
                        <br>
                        <?php
                    }
                ?>
            </div>
            <div class="delete">
                <a href="/admin/edit.php?mode=delete_task&task_id=<?php echo $task_id; ?>">
                    <button>Удалить задачу</button>
                </a>
            </div>
            <hr>
        <?php } ?>
        <h3>Новое задание:</h3>
        <form action="admin/edit.php" method="POST" enctype="multipart/form-data">
            <div class="task">
                <input type="hidden" name="mode" value="new_task">
                <!--                <input type="hidden" name="task_id" value="-->
                <?php //echo $task["task_id"]; ?><!--">-->
                <div class="title_div">
                    <span>Заголовок:</span>
                    <input type="text" name="title" value="">
                </div>
                <div class="textarea_div">
                    <span>Описание:</span>
                    <textarea name="descr" cols="30" rows="10"></textarea>
                </div>
                <div class="textarea_div">
                    <span>Описание при блокировке:</span>
                    <textarea name="lock_descr" cols="30" rows="10">На данный момент задание недоступно.</textarea>
                </div>
                <div class="button_config">
                    <span>Надпись на верхней кнопке:</span>
                    <input type="text" name="btn1_text" id="btn1_text" value="">
                    <span>Ссылка на верхней кнопке:</span>
                    <input type="text" name="btn1_link" id="btn1_link" value="">
                    <span></span>
                </div>


                <div style="margin: 1em 0;">
                    <input type="checkbox" name="blocked">
                    <label for="blocked">Заблокировать</label>
                </div>

                <hr>
                <div class="upload_file">
                    <span>Загрузить один файл:</span>
                    <input type="file" name="upload_file">
                </div>

                <div class="submit_div">
                    <input type="submit">
                </div>

            </div>
        </form>
    </div>
    <div id="settings" style="display: none;">
        <h2>Настройка сайта</h2>
        <?php
        $result = mysqli_query($link, "SELECT button2_text,button2_url,hello_text,iframe_link FROM config");
        $btn = mysqli_fetch_assoc($result);
        ?>
        <form action="admin/settings.php" method="POST" id="settingsForm">
            <span>Надпись на нижней кнопке:</span>
            <input type="text" name="btn2_text" id="btn2_text" value="<?php echo $btn["button2_text"]; ?>">
            <span>Ссылка на нижней кнопке:</span>
            <input type="text" name="btn2_link" id="btn2_link" value="<?php echo $btn["button2_url"]; ?>">
            <br>
            <hr>
            <textarea name="hello_text" id="hello_text" cols="30"
                      rows="10"><?php echo urldecode($btn["hello_text"]); ?></textarea>
            <input type="submit">
            <hr>
            <span>Ссылка на видео:</span>
            <input type="text" name="iframe_link" id="iframe_link" value="<?php echo $btn["iframe_link"]; ?>">
        </form>
        <div id="backup">
            <a href="/admin/backup.php">Сделать резервную копию</a>
            <?php if (file_exists("backup.zip")) { ?>
                <a href="/backup.zip">Резервная копия</a>
            <?php } ?>
            <a href="/admin/cleanup.php?mode=0">
                <button>Очистить базу данных и файлы</button>
            </a>
            <a href="/admin/cleanup.php?mode=1">
                <button>Очистить базу данных пользователей и сданную ими информацию</button>
            </a>
        </div>
    </div>
    <div id="call_requests_div" style="display: none;">
        <h2>Запрошенные звонки</h2>
        <button id="clear_calls">Очистить</button>
        <br>
        <table class="">
            <tr>
                <th>Время</th>
                <th>Имя из ВК</th>
                <th>Номер</th>
                <th>Статус</th>
                <th>Количество звонков</th>
                <th>Управление</th>
            </tr>
            <?php

            $call_rq_states = Array(
                0 => "Позвонить",
                1 => "Не взял(а)",
                2 => "Всё решили",
                3 => "Не решили"
            );

            $result = mysqli_query($link, "SELECT id,uid,ts,phone,state,cnt,name FROM call_requests ORDER BY ts DESC");

            while ($request = mysqli_fetch_assoc($result)) {
                ?>
                <tr data-id="<?php echo $request["id"]; ?>">
                    <td><?php echo $request["ts"]; ?></td>
                    <td><a href="https://vk.com/id<?php echo $request["uid"]; ?>"><?php echo $request["name"]; ?></a>
                    </td>
                    <td><a href="tel:<?php echo $request["phone"]; ?>"><?php echo $request["phone"]; ?></a></td>
                    <td class="state_td"><?php echo $call_rq_states[$request["state"]]; ?></td>
                    <td><span class="count"><?php echo $request["cnt"]; ?></span>
                        <button class="increase">+1</button>
                    </td>
                    <td class="state">
                        <button data-state="0" class="modifystate">Позвонить</button>
                        <button data-state="1" class="modifystate">Не взял(а)</button>
                        <button data-state="2" class="modifystate">Всё решили</button>
                        <button data-state="3" class="modifystate">Не решили</button>
                    </td>
                </tr>
                <?php
            }

            ?>
        </table>
    </div>
</div>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/jquery-confirm.js"></script>
<script src="js/oldadmin.js"></script>
</body>
</html>
