<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 29.07.18
 * Time: 19:48
 */

require_once "../auth.php";
require_once "../mysql_login.php";

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . "/" . $object))
                    rrmdir($dir . "/" . $object);
                else
                    unlink($dir . "/" . $object);
            }
        }
        rmdir($dir);
    }
}


mysqli_query($link, "DELETE FROM users");
rrmdir("../../files/work");