<?php

include "admin/auth.php";
include "admin/mysql_login.php";

function make_links_clickable($content)
{

    // The link list
    $links = array();

    // Links out of text links
    preg_match_all('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', $content, $matches);
    foreach ($matches[0] as $key => $link) {
        $links[$link] = $link;
    }

    // Get existing
    preg_match_all('/<a\s[^>]*href=([\"\']??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU', $content, $matches);
    foreach ($matches[2] as $key => $value) {
        if (isset($links[$value])) {
            unset($links[$value]);
        }
    }

    // Replace in content
    foreach ($links as $key => $link) {
        $content = str_replace($link, '<a href="' . $link . '" target="_blank">' . $link . '</a>', $content);
    }

//    $content = iconv("UTF-8","UTF-8", $content);
    return $content;
}

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админка</title>
    <link rel="stylesheet" href="/css/admin.css">
    <link rel="stylesheet" href="/css/jquery-confirm.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono|Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="/node_modules/noty/lib/noty.css">

    <!--    script[type="text/x-template" id] -->
    <script type="text/x-template" id="check_app">
        <div id="check" :class="{ empty: isTasksEmpty }" class="app">
            <task v-for="task in tasks" :task="task" v-if="task.state != 2" :key="task.ts + task.vk_uid"></task>
            <div v-show="loaded_cnt < cnt && state === 'loaded'" @click="load" id="load_btn">Загрузить дальше</div>
            <div v-if="state === 'loading'">Загрузка... <img src="/res/spinner.svg" height="1em" alt=""></div>
            <div v-if="state === 'empty'">Сданные задания закончились!</div>
            <img src="/res/refresh.png" alt="" @click="reload" class="refresh_btn">
        </div>
    </script>
    <script type="text/x-template" id="task_template">
        <div class='task'>
            <div class='controls'>
                <div>№{{ task.task_id }}.&nbsp;&nbsp;<a :href='"https://vk.com/id" + task.vk_uid'>
                        <img :src='task.avatar_uri || "/res/user.png"' alt=''>{{ task.name }}
                    </a></div>
                <div class="controls_btns">
                    <i class='material-icons ok-btn' @click='commit_ok' :class="{ activeButton: committing_type == 0 }">check</i>
                    <i class='material-icons not-ok-btn' @click='commit_not_ok' :class="{ activeButton: committing_type == 1 }">close</i>
                </div>
            </div>
            <div class="admintext" :class="{ short0: !committing }">
                <form action="#" :class="{ short0: !committing }">
                    <textarea name="admincomment" v-model="admin_comment" title=""></textarea>
                </form>
            </div>
            <div class='usertext' v-html='makeLinks(urldecode(task.submitted_text))'></div>
            <div class="userfile" v-if="shortfilename"><br><img src="/res/иконки/скрепка.png" style="height: 1em;"
                                                                alt="">&nbsp;&nbsp;<a
                        :href="'/download.php?filename=' + urldecode(task.filename)">{{ shortfilename }}</a></div>
            <div class="timestamp">{{ task.ts }}</div>
        </div>
    </script>

    <script type="text/x-template" id="stats_app">
        <div id="stats" :class="{ empty: tasks.length === 0 }" class="app">
            <task_stats :task="task" :task_id="task_id" v-for="(task, task_id) in tasks"
                        :key="'task' + task_id"></task_stats>
            <quiz_stats :quiz="quiz" :quiz_id="quiz_id" v-for="(quiz, quiz_id) in quizes"
                        :key="'quiz' + quiz_id"></quiz_stats>
            <div v-if="state === 'loading'">Загрузка...</div>
            <div v-if="state === 'empty'">Статистики нет, никто ничего не сдавал.</div>
        </div>
    </script>
    <script type="text/x-template" id="task_stats_template">
        <div class="task">
            <div class="title">Задание №{{ task_id }}</div>
            <div class="diagram">
                <div v-for="state in task">
                    <div class="line" :style="getLineStyle(state)">&nbsp;</div>
                    <div class="cnt">{{ state.cnt }}</div>
                </div>
            </div>
        </div>
    </script>
    <script type="text/x-template" id="quiz_stats_template">
        <div class="task">
            <div class="title">Тест №{{ quiz_id }}</div>
            <div class="diagram">
                <div v-for="state in quiz">
                    <div class="line" :style="getLineStyle(state)">&nbsp;</div>
                    <div class="cnt">{{ state.cnt }}</div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/x-template" id="menu_templ">
        <div id="mainmenu">
            <a href="/main.php">Смотреть сайт</a>
            <a :href="'#' + hash" @click="setMode" v-for="(text, hash) in buttons" :class="{ active: mode == hash }">{{
                text }}</a>
        </div>
    </script>

    <script type="text/x-template" id="user_template">
        <div class="user">
            <a :href="user.vk_uid > 0 ? 'https://vk.com/id' + user.vk_uid : '#'"><img
                        :src="user.avatar_uri || '/res/user.png'" alt=""></a>
            <a :href="'/admin/setuid.php?uid=' + user.vk_uid">{{ user.name != " " ? user.name : "null" }}</a>
            <a v-if="user.email" :href="'mailto:' + user.email">{{ user.email }}</a>
        </div>
    </script>
    <script type="text/x-template" id="users_app">
        <div id="users" class="app">
            <div class="center"><input type="text" v-model="query" id="search_query" title=""></div>
            <user v-for="user in users" :user="user"></user>
        </div>
    </script>

    <script type="text/x-template" id="calls_app">
        <div id="calls" class="app">
            <button>Очистить</button>
            <div class="task" v-for="(calls1, date) in calls" :key="date">
                <table>
                    <tr>
                        <th>Время</th>
                        <th>Имя в ВК</th>
                        <th>Телефон</th>
                        <th>Статус</th>
                        <th>Кол-во</th>
                        <th>Управление</th>
                    </tr>
                    <tr class="call" v-for="call in calls1" :key="call.id">
                        <td>{{ call.ts }}</td>
                        <td><a :href="call.href">{{ call.name }}</a></td>
                        <td><a :href="call.phonehref">{{ call.phone }}</a></td>
                        <td>{{ states[call.state] }}</td>
                        <td><span>{{ call.cnt }}</span>&nbsp;&nbsp;<button @click="inc_cnt(call)">+1</button>
                        </td>
                        <td class="controls">
                            <button @click="set_state(call, 0)">Позвонить</button>
                            <button @click="set_state(call, 1)">Не взял/а</button>
                            <button @click="set_state(call, 2)">Всё решили</button>
                            <button @click="set_state(call, 3)">Не решили</button>
                        </td>
                    </tr>
                </table>
                <div class="timestamp">{{ date }}</div>
            </div>
        </div>
    </script>

    <script type="text/x-template" id="settings_app">
        <div id="settings" class="app">
            <!--
            <div class="task">
                <div class="title">Нижняя кнопка:</div>
                <div class="config">
                    <input type="text" v-model="config.button2_text">
                    <input type="text" v-model="config.button2_url">
                </div>
            </div>
            -->
            <div class="task">
                <div class="title">Приветственное видео</div>
                <div class="config">
                    <textarea cols="60" rows="20" v-model="config.hello_text"></textarea>
                    <input type="text" v-model="config.iframe_link">
                </div>
            </div>
            <div class="task">
                <div class="title">Очистка базы данных</div>
                <div class="config">
                    <div class="button" @click="confirmClearUsers">Очистить все данные о пользователях</div>
                </div>
            </div>
            <div @click="submit" id="load_btn">Сохранить</div>
        </div>
    </script>

    <script type="text/x-template" id="select_app">
        <div id="select" class="app">
            <div id="filters">
                <table>
                    <tr>
                        <th rowspan="2">Заголовок</th>
                        <th colspan="5">Фильтры</th>
                    </tr>
                    <tr>
                        <td><img src="/res/иконки/часы.png" alt=""></td>
                        <td><img src="/res/иконки/галка.png" alt=""></td>
                        <td><img src="/res/иконки/часы.png" alt=""><img src="/res/иконки/галка.png" alt=""></td>
                        <td><img src="/res/иконки/замок_о.png" alt=""><img src="/res/иконки/крестик.png" alt=""></td>
                        <td></td>
                    </tr>
                    <tr v-for="_task in tasks">
                        <td>{{ _task.title }}</td>
                        <td><input type="radio" @change="change(_task.task_id, 1)" value="1" v-model="_task.filter">
                        </td>
                        <td><input type="radio" @change="change(_task.task_id, 2)" value="2" v-model="_task.filter">
                        </td>
                        <td><input type="radio" @change="change(_task.task_id, 12)" value="12" v-model="_task.filter">
                        </td>
                        <td><input type="radio" @change="change(_task.task_id, 3)" value="3" v-model="_task.filter">
                        </td>
                        <td><input type="radio" @change="change(_task.task_id, 0)" value="0" v-model="_task.filter">
                        </td>
                    </tr>
                </table>
                <div style="margin: 5px 0;">Принимаемая задача:</div>
                <select v-model="task">
                    <option v-for="_task in tasks" :value="_task" :key="_task.task_id">{{ _task.title }}</option>
                </select>
                <button @click="get_data">Получить список</button>
                <button class="clipboard" data-clipboard-target="#uids">Скопировать</button>
                <button @click="submit_task">Принять задачу</button>
                <textarea name="" id="dot" cols="30" rows="10" v-model="task.default_ok_text"></textarea>
            </div>
            <div>
                <div class="title">Количество: {{ users_cnt }}</div>
                <div id="uids">
                    <div v-for="user in users" :key="user.vk_uid" v-if="parseInt(user.vk_uid) > 0">{{ user.vk_uid }}&nbsp;
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/x-template" id="edit_app">
        <div id="edit" class="app">
            <editor v-for="task in tasks" :task="task" :key="task.task_id"></editor>
        </div>
    </script>

    <script type="text/x-template" id="editor_template">
        <form>
            <div class="task">
                <div class="title">Заголовок</div>
                <input type="text" v-model="task.title">
                <br>
                <div class="title">Описание</div>
                <textarea rows="20" v-model="task.descr"></textarea>
                <br>
                <div class="title">Описание при блокировке</div>
                <textarea rows="20" v-model="task.lock_descr"></textarea>
                <br>
                <div class="title">Надпись на верхней кнопке</div>
                <input type="text" v-model="task.button1_text">
                <br>
                <div class="title">Ссылка на верхней кнопке</div>
                <input type="text" v-model="task.button1_url">
                <br>
                <div class="title">Текст под кнопкой</div>
                <input type="text" v-model="task.button1_descr">
                <br>
                <div class="title">Файлы</div>
                <input type="file" :id="'newfile' + task.task_id" name="newfile" class="hidden" @change="watchNewFile">
                <label :for="'newfile' + task.task_id"><img src="/res/иконки/скрепка.png" alt="">&nbsp;{{ newfile
                    }}</label>
                <div class="warning" v-show="noFileWarning">Не указан файл!</div>
                <br>
                <div class="files">
                    <file v-for="file in files" :file="file" :task="task" :key="file"></file>
                </div>
                <div class="blocker">
                    <input class="switcher__input" type="checkbox" :id="'switcher' + task.task_id"
                           v-model="task.blocked">
                    <label class="switcher__label" :for="'switcher' + task.task_id">
                        <span class="switcher__text">Блокировать</span>
                    </label>
                </div>
                <br>
                <div class="title">Текст для принятия</div>
                <input type="text" v-model="task.default_ok_text">
                <br>
                <div class="title">Текст для отказа</div>
                <input type="text" v-model="task.default_not_ok_text">
                <div class="button" @click="submit">Сохранить задачу</div>
            </div>
        </form>
    </script>

    <script type="text/x-template" id="file_template">
        <div>
            <a :href="'/download.php?filename=files/task/' + task.task_id + '/' + file" target="_blank">{{ file }}</a>&nbsp;&nbsp;&nbsp;
            <a href="#edit" @click="deleteFile">(удалить)</a>
        </div>
    </script>

    <script type="text/x-template" id="quiz_app">
        <div id="quiz" class="app">
            <div class="task">
                <div class="title">Заголовок</div>
                <input type="text" v-model="quiz.quiz_title">
                <br>
                <div class="title">Минимальная принятая задача для доступа к тесту</div>
                <input type="number" v-model="quiz.min_task_done">
                <br>
                <div class="button" @click="submit">Сохранить</div>
            </div>
        </div>
    </script>

</head>
<body>
<div id="main_container">
    <menu_templ :buttons="modes" :mode="mode"></menu_templ>

    <checkapp v-if="mode == 'check'"></checkapp>
    <statsapp v-if="mode == 'stats'"></statsapp>
    <usersapp v-if="mode == 'users'"></usersapp>
    <callsapp v-if="mode == 'calls'"></callsapp>
    <settingsapp v-if="mode == 'settings'"></settingsapp>
    <selectapp v-if="mode == 'select'"></selectapp>
    <editapp v-if="mode == 'edit'"></editapp>
    <quizapp v-if="mode == 'quiz'"></quizapp>
</div>
<!-- development version, includes helpful console warnings -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://rawgit.com/xiaoluoboding/vue-stroll/master/dist/vue-stroll.min.js"></script>
<script src="js/fontawesome.js"></script>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/jquery-confirm.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="https://unpkg.com/clipboard@2.0.0/dist/clipboard.min.js"></script>
<script src="/node_modules/noty/lib/noty.js"></script>
<script src="js/admin.js"></script>
</body>
</html>
