<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 02.04.18
 * Time: 12:20
 */

require_once "mysql_login.php";
require_once "auth.php";

$result = mysqli_query($link, "SELECT task_id, title FROM tasks");
?>
<table id="ajaxselect">
    <tr>
        <th>Заголовок</th>
        <th><img src="/res/иконки/галка.png" alt=""></th>
        <th><img src="/res/иконки/часы.png" alt=""></th>
        <th><img src="/res/иконки/часы.png" alt=""><img src="/res/иконки/галка.png" alt=""></th>
        <th><img src="/res/иконки/замок_з.png" alt=""><img src="/res/иконки/замок_о.png" alt=""><img
                    src="/res/иконки/крестик.png" alt=""></th>
    </tr>
    <?php
    while ($task = mysqli_fetch_assoc($result)) {
        $title = $task["title"];
        $taskid = $task['task_id'];
        echo <<<MYTAG
    <tr>
        <td class="expand">$title</td>
        <td><input type="radio" value="2"   data-id="$taskid" name="task[$taskid]"></td>
        <td><input type="radio" value="1"   data-id="$taskid" name="task[$taskid]"></td>
        <td><input type="radio" value="1,2" data-id="$taskid" name="task[$taskid]"></td>
        <td><input type="radio" value="0,3" data-id="$taskid" name="task[$taskid]"></td>
        <td><input type="radio" value="-"   data-id="$taskid" name="task[$taskid]" checked></td>
    </tr>
MYTAG;

    }

    ?>
</table>

<label for="task_select">Принять задание:&nbsp;</label>
<select name="task_select" id="task_select">
    <?php
    $result = mysqli_query($link, "SELECT task_id, title FROM tasks");

    while ($task = mysqli_fetch_assoc($result)) {
        $taskid = $task["task_id"];
        $title = $task["title"];

        echo "<option value='$taskid'>$title</option>";
    }
    ?>
</select>

<style>
    #ajaxselect {
        border-collapse: collapse;
    }

    #ajaxselect td, #ajaxselect th {
        padding: 0.6em;
        border: solid black 1px;
    }

    td.expand {
        width: 100%;
    }
</style>

<!--suppress JSUnusedLocalSymbols -->
<script>
    var users_data = null;

    function ajax_show() {
        var data = [];
        document.querySelectorAll("input[type=radio]").forEach(function (value) {
            if (value.checked !== false)
                data.push(value.dataset.id + "|" + value.value);
        });
        data = {"data": data};
        console.log(data);
        $.ajax({
            url: "/admin/show_selected_users.php",
            data: data,
            method: "post",
            success: function (data, textStatus, jqXHR) {
                data = JSON.parse(data);
                $.confirm({
                    title: "Выбрано " + data["cnt"] + " пользователей.",
                    content: data["html"],
                    useBootstrap: false,
                    boxWidth: "400px",
                    backgroundDismiss: true,
                    escapeKey: true,
                    buttons: {
                        OK: {}
                    }
                })
            }
        });
        return false;
    }

    function ajax_accept() {
        var data = [];
        document.querySelectorAll("input[type=radio]").forEach(function (value) {
            if (value.checked !== false)
                data.push(value.dataset.id + "|" + value.value);
        });
        data = {"data": data};
        console.log(data);
        $.ajax({
            url: "/admin/show_selected_users.php",
            data: data,
            method: "post",
            success: function (data, textStatus, jqXHR) {
                users_data = JSON.parse(data);
                new_id = document.getElementById("task_select").options[document.getElementById("task_select").selectedIndex].text;
                $.confirm({
                    title: "Подтвердите действие.",
                    content: 'Выбрано ' + users_data["cnt"] + ' пользователей.<br>Принять им всем дз "' + new_id + '"',
                    useBootstrap: false,
                    boxWidth: "400px",
                    backgroundDismiss: true,
                    escapeKey: true,
                    buttons: {
                        OK: {
                            text: "Подтвердить",
                            btnClass: "btn-green",
                            action: function (e) {
                                $.ajax({
                                    url: "/admin/accept_task.php",
                                    method: "post",
                                    data: {
                                        "task_id": document.getElementById("task_select").value,
                                        "uids": users_data["html"].split("<br>").join(" "),
                                    },
                                    success: function (data, textStatus, jqXHR) {
                                        $.confirm({
                                            type: "green",
                                            title: "Операция завершена",
                                            content: data,
                                            useBootstrap: false,
                                            boxWidth: "300px",
                                            buttons: [
                                                {
                                                    text: "OK"
                                                }
                                            ]
                                        })
                                    }
                                })
                            }
                        },
                        cancel: {
                            text: "Отменить",
                            btnClass: "btn-red"
                        }
                    }
                })
            }
        });
        return false;
    }

</script>
