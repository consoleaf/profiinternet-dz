<?php
/**
 * Created by PhpStorm.
 * User: takanashi
 * Date: 14.05.18
 * Time: 16:53
 */


set_error_handler('err_handler');
function err_handler($errno, $errmsg, $filename, $linenum)
{
    $date = date('Y-m-d H:i:s (T)');
    $f = fopen('errorss.txt', 'a');
    if (!empty($f)) {
        $filename = str_replace($_SERVER['DOCUMENT_ROOT'], '', $filename);
        $err = "$errmsg = $filename = $linenum\r\n";
        fwrite($f, $err);
        fclose($f);
    }
}

if (isset($_POST["quiz_id"]))
    $quiz_id = $_POST["quiz_id"];
else
    die("Не указан номер теста.");
session_start();
require_once "admin/mysql_login.php";
if (!isset($_SESSION["uid"]))
    $uid = $_POST["uid"];
else
    $uid = $_SESSION["uid"];
$min_score = mysqli_fetch_assoc(mysqli_query($link, "SELECT min_score FROM quizes WHERE quiz_id = $quiz_id"))["min_score"];

error_log(mysqli_error($link));

mysqli_query($link, "DELETE FROM users_quizes  WHERE quiz_id = $quiz_id AND vk_uid = $uid");
mysqli_query($link, "DELETE FROM users_answers WHERE quiz_id = $quiz_id AND vk_uid = $uid");

$score = 0;
$max_score = 0;

$questions = $_POST["question"];

foreach ($questions as $question_id => $answer_id) {
    $correct = mysqli_fetch_assoc(mysqli_query($link,
        "SELECT correct_answer_id aid FROM quizes_questions WHERE question_id = $question_id"))["aid"];
    mysqli_query($link,
        "INSERT INTO users_answers(vk_uid, answer_id, quiz_id) VALUE ($uid, $answer_id, $quiz_id)");
    if ($correct === $answer_id)
        $score++;
    $max_score++;
}

echo "<h2>Ваш результат: " . $score . "/" . $max_score . ". Вы " . ($score >= $min_score ? "успешно сдали" : "не прошли") . " тест.</h2>";

error_log(mysqli_error($link));

mysqli_query($link, "INSERT INTO users_quizes(vk_uid, quiz_id, score) VALUE ($uid, $quiz_id, $score)");


// Submit next task after minimal necessary to get to the quiz

$min_task = mysqli_query($link, "SELECT min_task_done FROM quizes WHERE quiz_id = $quiz_id");
$min_task = mysqli_fetch_assoc($min_task)["min_task_done"] + 1;

//mysqli_query($link, "DELETE FROM users_tasks WHERE vk_uid = $uid AND task_id = $min_task");
$result = mysqli_query($link, "SELECT FROM users_tasks WHERE task_id = $min_task AND vk_uid = $uid");
if (mysqli_num_rows($result))
    mysqli_query($link, "UPDATE users_tasks SET state = 2, submitted_text = 'Тест пройден' WHERE vk_uid = $uid AND task_id = $min_task");
else
    mysqli_query($link, "INSERT INTO users_tasks(vk_uid, task_id, state, filename, admin_comment, submitted_text) VALUE ($uid, $min_task, 2, '', '', 'Тест пройден')");


?>
<script>
    setTimeout(function () {
        window.location.href = "/quiz.php?id=<?php echo $quiz_id; ?>&uid=<?php echo $uid; ?>";
    }, 5000)
</script>
