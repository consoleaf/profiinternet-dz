<?php

include_once "mysql_login.php";
include_once "auth.php";

$icons = Array(-1 => "замок_з", 0 => "замок_о", 1 => "часы", 2 => "галка", 3 => "крестик");

$result = mysqli_query($link, "SELECT count(vk_uid) cnt, task_id, state, max(ts) last_ts 
FROM users_tasks GROUP BY task_id, state
");

$result1 = mysqli_query($link, "SELECT count(vk_uid) cnt FROM users_quizes uq JOIN quizes q ON q.quiz_id = uq.quiz_id WHERE uq.score < q.min_score");
$result2 = mysqli_query($link, "SELECT count(vk_uid) cnt FROM users_quizes uq JOIN quizes q ON q.quiz_id = uq.quiz_id WHERE uq.score >= q.min_score");

$i = 1;
while ($cnt1 = mysqli_fetch_assoc($result1)["cnt"]) {
    $cnt2 = mysqli_fetch_assoc($result2)["cnt"];
    echo "<h2>$cnt2 человек сдало тест №$i успешно, $cnt1 - неуспешно.</h2>";
    $i++;
}

?>
<table>
    <tr>
        <th>Задача</th>
        <th>Состояние</th>
        <th>Количество</th>
        <th>Последняя сдача</th>
    </tr>
    <?php
    while ($row = mysqli_fetch_assoc($result)) {
        ?>
        <tr>
            <td><?php echo $row["task_id"]; ?></td>
            <td><img src="/res/иконки/<?php echo $icons[$row["state"]]; ?>.png" alt=""></td>
            <td><?php echo $row["cnt"]; ?></td>
            <td><?php echo $row["last_ts"]; ?></td>
        </tr>
        <?php
    }
    ?>
</table>


<style>
    table td {
        border: 1px solid black;
    }
</style>
