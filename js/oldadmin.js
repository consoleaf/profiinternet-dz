$(function () {
    $("#settings_button").click(function () {
        $("#checking").hide();
        $("#editing").hide();
        $("#settings").show();
        $("#call_requests_div").hide();
    });
    $("#editing_button").click(function () {
        $("#checking").hide();
        $("#editing").show();
        $("#settings").hide();
        $("#call_requests_div").hide();
    });
    $("#checking_button").click(function () {
        $("#checking").show();
        $("#editing").hide();
        $("#settings").hide();
        $("#call_requests_div").hide();
    });

    $("#call_requests").click(function () {
        $("#checking").hide();
        $("#editing").hide();
        $("#settings").hide();
        $("#call_requests_div").show();
    });
    // $("#btn_link").mask("http://");
    $("#btn_link").bind("keyup", function (e) {
        // console.log(e.key);
        if ($("#btn_link")[0].value.substr(0, 7) !== "http://" &&
            $("#btn_link")[0].value.substr(0, 8) !== "https://")
            $("#btn_link")[0].value = "http://" + $("#btn_link")[0].value.substr(8, $("#btn_link")[0].value.length - 7);
    });
    $(".task input[type=submit]").click(function () {
        document.body.style.cursor = "progress";
    })
});


// this is the id of the form
$("#settingsForm").submit(function (e) {

    var url = "admin/settings.php"; // the script where you handle the form input.

    $.ajax({
        type: "POST",
        url: url,
        data: $("#settingsForm").serialize(), // serializes the form's elements.
        success: function (data) {
            if (data === "success")
                $.confirm({
                    useBootstrap: false,
                    boxWidth: "500px",
                    type: "green",
                    title: "Успешно изменено!",
                    content: "",
                    typeAnimated: true,
                    buttons: {
                        ok: {
                            text: "OK",
                            type: "green",
                            action: function () {

                            }
                        }
                    }
                });
            else
                $.confirm({
                    useBootstrap: false,
                    boxWidth: "500px",
                    type: "red",
                    title: "Произошла ошибка.",
                    content: data,
                    typeAnimated: true,
                    buttons: {
                        ok: {
                            text: "OK",
                            type: "red",
                            action: function () {

                            }
                        }
                    }
                });
        }
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});

var click_confirmed = false;

function clickConfirm(e) {
    if (!click_confirmed)
        e.preventDefault();
    else
        return true;
    $.confirm({
        title: "Подтвердите действие и нажмите ещё раз.",
        useBootstrap: false,
        boxWidth: "400px",
        buttons: {
            yes: {
                text: "Подтвердить",
                btnClass: "btn-green",
                action: function () {
                    click_confirmed = true;
                }
            },
            no: {
                text: "Отменить",
                btnClass: "btn-red",
                action: function () {

                }
            }
        }
    });
}

$("input[type=submit], div.delete, #backup button").bind("click", clickConfirm);

let admin_comment;

$("a.result").click(function (e) {
    let task_id = e.currentTarget.dataset.task_id;
    let vk_uid = e.currentTarget.dataset.vk_uid;
    let result = e.currentTarget.dataset.result;
    window.currentTarget = e.currentTarget;
    console.log(result);
    $.confirm({
        title: "Комментарий:",
        boxWidth: "60%",
        useBootstrap: false,
        content: "<textarea id='comment' style='width: 99%; height: 100%;' rows='28'></textarea>",
        escapeKey: "cancel",
        backgroundDismiss: "cancel",
        onOpen: function () {
            document.querySelector("#comment").focus();
        },
        buttons: {
            ok: {
                text: "OK",
                btnClass: "btnBlue",
                action: function (e) {
                    window.admin_comment = document.getElementById("comment").value;
                    console.log(window.admin_comment);
                    $.ajax({
                        url: "admin/check.php",
                        dataType: 'json',
                        method: 'post',
                        data: {
                            task_id: task_id,
                            vk_uid: vk_uid,
                            result: result,
                            text: encodeURIComponent(window.admin_comment)
                        },
                        complete: function (response) {
                            console.log(response);
                            $.confirm({
                                boxWidth: "400px",
                                useBootstrap: false,
                                title: $(response.responseText).filter("title").text(),
                                content: response.responseText,
                                backgroundDismiss: true,
                                escapeKey: "OK",
                                buttons: {
                                    OK: {
                                        text: "OK",
                                        keys: ["enter"],
                                        action: function () {
                                            console.log(window.currentTarget);
                                            window.currentTarget.parentElement.parentNode.style.display = "none";
                                        }
                                    }
                                }
                            })
                        }
                    })
                }
            },
            cancel: {
                text: "Отмена",
            }
        }
    });
});

$(".makeBold").click(function (e) {
    e.preventDefault();
    let textarea = e.currentTarget.parentNode.parentNode.querySelector("textarea");
    let text = textarea.innerHTML;
    let prev = text.substr(0, textarea.selectionStart);
    let mid = text.substr(textarea.selectionStart, textarea.selectionEnd - textarea.selectionStart);
    let past = text.substr(textarea.selectionEnd, text.length);

    mid = "<b>" + mid + "</b>";

    textarea.innerHTML = prev + mid + past;
});


$("#stats").click(function (e) {
    $.confirm({
        backgroundDismiss: true,
        escapeKey: true,
        title: 'Статистика',
        content: 'url:admin/stats.php',
        onContentReady: function () {
            var self = this;
        },
        columnClass: 'medium',
        useBootstrap: false,
        boxWidth: "400px",
    });
});

$("#select").click(function (e) {
    $.confirm({
        backgroundDismiss: true,
        escapeKey: true,
        title: "Выборка пользователей",
        content: "url:admin/select.php",
        columnClass: 'medium',
        useBootstrap: false,
        boxWidth: "600px",
        buttons: {
            accept: {
                action: function (e) {
                    return ajax_accept();
                },
                text: "Принять д/з и отобразить"
            },
            show: {
                action: function (e) {
                    return ajax_show();
                },
                text: "Отобразить"
            }
        }
    })
});

$("button.increase").click(function (e) {
    let tr = e.currentTarget.parentNode.parentNode;
    let id = tr.attributes.getNamedItem("data-id").nodeValue;
    let span = tr.querySelector("span.count");
    $.ajax({
        method: "post",
        url: "/admin/inc_call_cnt.php",
        data: {
            "id": id
        },
        success: function (data) {
            span.innerHTML = data;
        }
    })
});

$("button#clear_calls").click(function (e) {
    $.ajax({
        method: "post",
        url: "/admin/clear_calls.php",
        success: function (e) {
            window.location.reload();
        }
    })
});

$("button.modifystate").click(function (e) {
    let tr = e.currentTarget.parentNode.parentNode;
    let id = tr.attributes.getNamedItem("data-id").nodeValue;
    let td = tr.querySelector("td.state_td");
    let state = e.currentTarget.getAttribute("data-state");
    $.ajax({
        url: "/admin/modifystate.php",
        method: "post",
        data: {
            "id": id,
            "state": state
        },
        success: function (data) {
            td.innerHTML = {
                0: "Позвонить",
                1: "Не взял(а)",
                2: "Всё решили",
                3: "Не решили"
            }[state];
        }
    })
});
